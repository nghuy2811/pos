console.log('Socket server is started');
var express = require("express");
var app = express();
 
var server = require("http").Server(app);
var io = require("socket.io")(server);
server.listen(3000);
 
io.on("connection", function(socket){
	console.log('connected');
	socket.on('order', function(msg){
		io.emit('order', msg);
	});
	socket.on('login', function(msg){
		io.emit('login', msg);
	});
	socket.on('closepos', function(msg){
		io.emit('closepos', msg);
	});
	socket.on('updaterabatt', function(msg){
        io.emit('updaterabatt', msg);
    });
    socket.on('openPriceInfoCS', function(msg){
        io.emit('openPriceInfoCS', msg);
    });
    socket.on('closePriceInfoCS', function(msg){
        io.emit('closePriceInfoCS', msg);
    });
    socket.on('refreshCS', function(msg){
        io.emit('refreshCS', msg);
    });
    socket.on('ask_socket_login', function(msg){
        socket.broadcast.emit('ask_socket_login', msg);
    });
    socket.on('alreadylogin', function(msg){
        socket.broadcast.emit('alreadylogin', msg);
    });
	socket.on('connect_failed', function() {
		console.log('connect fail');
		delete socket;
	});
	socket.on('error', function() {
		console.log('error');
		delete socket;
	});
	socket.on('reconnect_failed', function() {
		console.log('reconnect fail');
		delete socket;
	});
	socket.on('disconnect', function() {
		console.log('disconnected');
		delete socket;
	});
});
