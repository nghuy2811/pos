import injector from 'vue-inject'
import commonService from '../services/common.service.js'
import productService from '../services/product.service.js'

injector.service('commonService', () => {
  return commonService;
});

injector.service('productService', () => {
  return productService;
});