// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import injector from 'vue-inject';
import VueCookies from 'vue-cookies';
import store from './store';
import JsonExcel from 'vue-json-excel';
import VueMoment from 'vue-moment';
import VueTouchKeyboard from 'vue-touch-keyboard';
import i18n from './i18n';
import lzutf8 from 'lzutf8';
import './filters/filters';
// import IdleVue from 'idle-vue'
import VueBarcode from 'vue-barcode';


import { globalStore } from './global/global.js';

import style from "vue-touch-keyboard/dist/vue-touch-keyboard.css"

require('./injector/injector-registor');

const eventsHub = new Vue();

// Vue.use(IdleVue, {
//   eventEmitter: eventsHub,
//   idleTime: 15 * 60 * 1000
// })

Vue.use(require('vue-moment'));
Vue.use(lzutf8);
Vue.use(VueCookies);
Vue.use(injector);
Vue.use(VueTouchKeyboard);
Vue.component('barcode', VueBarcode);

Vue.component('downloadExcel', JsonExcel);

Vue.config.productionTip = false;

Vue.filter('currency', {
  read: function (value) {
    return '$' + value.toFixed(2)
  },
  write: function (value) {
    var number = +value.replace(/[^\d.]/g, '')
    return isNaN(number) ? 0 : number
  }
});

Vue.filter('truncate', function(value, limit, clamp) {
  if (value.length > limit) {
    if(clamp.length <= 0) {
      clamp = '...';
    }
    value = value.substring(0, (limit - 3)) + clamp;
  }
  return value;
});

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app');
