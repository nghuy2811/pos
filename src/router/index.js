import Vue from 'vue'
import Router from 'vue-router'
const Home = () => import('../components/Home.vue');
const Login = () => import('../components/Login.vue');
const Payment = () => import('../components/Payment.vue');
const Print = () => import('../components/Print.vue');
const OfferPrint = () => import('../components/OfferPrint.vue');
const ListOrdered = () => import('../components/ListOrdered.vue');
const ReturnDetails = () => import('../components/ReturnDetails.vue');
const ReloadDetails = () => import('../components/ReloadDetails.vue');
const Bericht = () => import('../components/Bericht.vue');
const Customer = () => import('../components/Customer.vue');
const Weight = () => import('../components/Weight.vue');
const ListCustomer = () => import('../components/ListCustomer.vue');
const ListOrderedChef = () => import('../components/ListOrderedChef.vue');
const ListOrderedChefExport = () => import('../components/ListOrderedChefExport.vue');
const ListOfferedChef = () => import('../components/ListOfferedChef.vue');
const ListOfferedChefExport = () => import('../components/ListOfferedChefExport.vue');
const NegativeProduct = () => import('../components/NegativeProduct.vue');
const DetailOrderedChef = () => import('../components/DetailOrderedChef.vue');
const DetailOrderedChefExport = () => import('../components/DetailOrderedChefExport.vue');
const DetailOfferedChef = () => import('../components/DetailOfferedChef.vue');
const DetailOfferedChefExport = () => import('../components/DetailOfferedChefExport.vue');
const DetailNegative = () => import('../components/DetailNegative.vue');
const DetailOffer = () => import('../components/DetailOffer.vue');
const OfferReport = () => import('../components/OfferReport.vue');
const NewCustomer = () => import('../components/NewCustomer.vue');
const CustomerDetails = () => import('../components/CustomerDetails.vue');

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [{
      path: "/index.html",
      redirect: "/"
    },
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/payment',
      name: 'Payment',
      component: Payment
    },
    {
      path: '/print',
      name: 'Print',
      component: Print
    },
    {
      path: '/offerprint',
      name: 'OfferPrint',
      component: OfferPrint
    },
    {
      path: '/list-ordered',
      name: 'ListOrdered',
      component: ListOrdered
    },
    {
        path: '/list-orderedchef',
        name: 'ListOrderedChef',
        component: ListOrderedChef
    },
    {
      path: '/list-orderedchef-export',
      name: 'ListOrderedChefExport',
      component: ListOrderedChefExport
    },
    {
      path: '/list-offeredchef',
      name: 'ListOfferedChef',
      component: ListOfferedChef
    },
    {
      path: '/list-offeredchef-export',
      name: 'ListOfferedChefExport',
      component: ListOfferedChefExport
    },
    {
      path: '/list-negativeproduct',
      name: 'NegativeProduct',
      component: NegativeProduct
    },
    {
        path: '/detail-orderedchef',
        name: 'DetailOrderedChef',
        component: DetailOrderedChef
    },
    {
      path: '/detail-orderedchef-export',
      name: 'DetailOrderedChefExport',
      component: DetailOrderedChefExport
    },
    {
      path: '/detail-offeredchef',
      name: 'DetailOfferedChef',
      component: DetailOfferedChef
    },
    {
      path: '/detail-offeredchef-export',
      name: 'DetailOfferedChefExport',
      component: DetailOfferedChefExport
    },
    {
      path: '/detail-negative',
      name: 'DetailNegative',
      component: DetailNegative
    },
    {
      path: '/detail-offer',
      name: 'DetailOffer',
      component: DetailOffer
    },
    {
      path: '/order-details/:id',
      name: 'ReturnDetails',
      component: ReturnDetails
    },
    {
      path: '/reload-details/:id',
      name: 'ReloadDetails',
      component: ReloadDetails
    },
    {
      path: '/bericht',
      name: 'Bericht',
      component: Bericht
    },
    {
      path: '/offer',
      name: 'OfferReport',
      component: OfferReport
    },
    {
      path: '/customer',
      name: 'Customer',
      component: Customer
    },
    {
      path: '/weight',
      name: 'Weight',
      component: Weight
    },
    {
      path: '/list-customer',
      name: 'ListCustomer',
      component: ListCustomer
    },
    {
      path: '/new-customer',
      name: 'NewCustomer',
      component: NewCustomer
    },
    {
      path: '/customer-details',
      name: 'CustomerDetails',
      component: CustomerDetails
    }
  ]
})
