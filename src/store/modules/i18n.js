import {
  globalStore
} from '@/global/global.js';

const state = { // data
  lang: localStorage.getItem('currentLang') !== null ? localStorage.getItem('currentLang').replace(/"/g, '') : 'vi'
};

const getters = { // computed
};

const actions = { // methods
};

const mutations = { // handle response from actions to update state
  changeLanguage(state, langParam) {
    state.lang = langParam;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
