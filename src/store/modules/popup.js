import { globalStore } from '@/global/global.js';
import router from '../../router';
import VueCookies from 'vue-cookies';
import axios from 'axios';

const state = { // data
    confirmType: '', //Confirm type when show password
    warningContent: '',
    txtpassword: '',
    canClick: true,
};

const getters = { // computed

};

const actions = { // methods
    confirmPopup({commit, state, dispatch, rootState}, begin_amount){
        let userinfo = JSON.parse(localStorage.getItem(globalStore.cookieNameLoginInfo)).value[0];
        let begintime = globalStore.$moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
        const postBody = {
            "params": {
                "userid": userinfo.userid,
                "username": userinfo.username,
                "begintime": begintime,
                "begin_amount": begin_amount || 0
            }
        };
        axios.post(`${globalStore.baseUrl}session/open_by_user`, postBody)
            .then(response => {
                let ret = JSON.parse(response.data.result);
                let session = JSON.parse(ret.value);
                if(session.length === 1){
                    rootState.product.currentSesssion = session[0].id;
                }
            });
    },
    closeConfirmPopup({commit, state, dispatch}, type){
        VueCookies.remove(globalStore.cookieNameLoginInfo);
        // localStorage.clear();
        router.push('/');
    },
};

const mutations = { // handle response from actions to update state
    handleCanClickBtnConfirmPopup(state, value) {
        state.canClick = value;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}
