import { globalStore } from "@/global/global.js";
import router from "../../router";
import axios from "axios";
import { log } from "lzutf8";

const state = {
  // data
  allProduct: [],
  reloadProductList: [], //Dùng để lấy danh sách reload sản phẩm
  productsFiltering: [],

  newOrder: {},

  // Data for Order
  orders: [],
  orderselectedindex: 0, // Dùng để đanh dấu order nào đang được chọn
  currentOrder: {},
  currentOrderSocket: {},
  // productSelected: [], //Đây là orderlines
  currentOrderline: {},

  selectedIndex: 0, // Dùng để đánh dấu click vào dòng số mấy và làm việc
  statebuffer: "menge", // State dùng để check đang ở menge, discount, price
  buffer: "",
  lineDiscountPercentage: false, // Biến này để check discountline là discount tiền hay %
  linePriceTotal: false, // Biến này để check là set giá tổng cho sản phẩm

  orderToRetoure: "", // Dùng để lưu lại order để retoure

  // data handle scroll list product
  totalProduct: 0,
  productInRow: 6,
  currentProductItemScrollTo: 0,

  currentSesssion: "", // ID Dùng để lưu session đang đăng nhập vào trong hệ thống
  currentSesssionName: "",

  //data handle scroll list product order
  totalProductOrder: 0,
  productInview: 6,
  currentProudctScrollInListOrder: 0,

  showPriceInfo: false, // Biến lưu popup price info có show hay không
  currentProductShowPopupInfo: {}, // Biến lưu thông tin sản phẩm hiện tại để show lên popup price info

  warningTotalPrice: false,

  currentProductKg: {},
  productWeight: "",
  productWeightPrice: "",
  isInputWeightFocus: false,
  isInputWeightPriceFocus: false,

  currentProductDiv: {},
  productDivMenge: "",
  productDivPrice: "",
  isInputDivMenge: false,
  isInputDivAmount: false,

  isShowKeyBoardSearchProduct: false, // Biến show hide bàn phím search product ở home

  orderlineIsHighlight: 0, // Biến lưu dòng hiện tại của list orderline đang được chọn

  isBeginAmountStarted: true,

  allProductLazyLoad: [],
  productsPerPage: 18,

  pagerProductList: 1, // Biến sử dụng cho phân trang của list product
  isActiveRefreshApp: true,

  checkRefreshLogin: false, // biến dùng cho auto reload khi về page login
  checkServer: true, // check thử có cần check server hay không

  isActiveQuantityPopup: false, // state for increase quantity popup, fix currentOrder.lines undefined issue
  quantityBuffer: "", // state for increase quantity popup input
};

const getters = {
  // computed
};

const actions = {
  // methods
  async initOrder({ commit, state, dispatch }, origin) {
    if (origin === "") {
      if (state.orders.length === 0 && state.checkServer == true) {
        state.orders = await globalStore.getOrderSyncServer();
        state.checkServer = false;
      }
      if (state.orders.length === 0) {
        await dispatch("addNewOrder");
        state.orderselectedindex = state.orders.length - 1;
        state.currentOrder = state.orders[state.orderselectedindex];
      } else {
        let checkblankorder = await dispatch("check_have_blankorder");
        state.orderselectedindex = state.orders.length - 1;
        state.currentOrder = state.orders[state.orderselectedindex];
        let max_order = await globalStore.getMaxOrder();
        if (state.orders.length <= max_order) {
          if (!checkblankorder) {
            await dispatch("addNewOrder");
            state.currentOrder = state.orders[state.orderselectedindex];
            state.currentOrder.return_back = 0;
            state.statebuffer = "menge";
          } else {
            for (let i = 0; i < state.orders.length; i++) {
              if (state.orders[i].lines.length === 0) {
                state.orderselectedindex = i;
              }
            }
            if (state.currentOrder.lines.length > 0) {
              dispatch("handleClickOnOrderLine", [
                0,
                state.currentOrder.lines[0],
              ]);
            }
          }
        }
      }
    }
    if (origin === "back") {
      if (state.orders.length === 0) {
        window.location.href = "/home";
      }
      state.currentOrder = state.orders[state.orderselectedindex];
      state.currentOrder.return_back = 0;
      // rootState.customer.
      if (state.currentOrder.lines.length > 0) {
        dispatch("handleClickOnOrderLine", [0, state.currentOrder.lines[0]]);
      }
    }
    let orderref = state.orders[state.orderselectedindex].ref;
    state.orders = state.orders.sort(function(a, b) {
      return (
        parseInt(a.ref.toString().substr(a.ref.toString().length - 7)) -
        parseInt(b.ref.toString().substr(b.ref.toString().length - 7))
      );
    });
    state.orderselectedindex = state.orders.findIndex(
      (p) => p.ref === orderref
    );
    state.lineDiscountPercentage = false;
    state.statebuffer = "menge";
  },
  async addNewOrder({ commit, state, dispatch }, allow = false) {
    commit("resetNewOrder");
    // Ở trong cái order trống thì không được thêm cái order nữa
    for (let i = 0; i < state.orders.length; i++) {
      if (
        state.orders[i].lines.length === 0 ||
        typeof state.orders[i].lines.length === "undefined"
      ) {
        return;
      }
    }

    let max_order = await globalStore.getMaxOrder();
    if (state.orders.length < max_order || allow) {
      state.orders.push(state.newOrder);
      state.orderselectedindex = state.orders.length - 1;
      state.currentOrder = state.orders[state.orderselectedindex];
      state.statebuffer = "menge";
      state.currentOrderSocket = JSON.stringify(state.currentOrder);
      let userinfo = JSON.parse(
        localStorage.getItem(globalStore.cookieNameLoginInfo)
      ).value[0];
      globalStore.sendSocket(
        state.currentOrderSocket,
        userinfo.username.toString()
      );
    }
  },
  addNewOrderWithPassword({ commit, state }) {
    commit("resetNewOrder");
    for (let i = 0; i < state.orders.length; i++) {
      if (
        state.orders[i].lines.length === 0 ||
        typeof state.orders[i].lines.length === "undefined"
      ) {
        return;
      }
    }
    // Cứ thấy retoure là thêm order mới và return nó ra luôn
    state.orders.push(state.newOrder);
    state.orderselectedindex = state.orders.length - 1;
    state.currentOrder = state.orders[state.orderselectedindex];
    let currenttime = new Date().getTime();
    let userinfo = JSON.parse(
      localStorage.getItem(globalStore.cookieNameLoginInfo)
    ).value[0];
    state.currentOrder.ref =
      userinfo.username.toString() + currenttime.toString();
    state.statebuffer = "menge";
  },
  addNewOrderRetoure({ commit, state }) {
    commit("resetNewOrder");
    // Cứ thấy retoure là thêm order mới và return nó ra luôn
    state.orders.push(state.newOrder);
    state.orderselectedindex = state.orders.length - 1;
    state.currentOrder = state.orders[state.orderselectedindex];
    let currenttime = new Date().getTime();
    let userinfo = JSON.parse(
      localStorage.getItem(globalStore.cookieNameLoginInfo)
    ).value[0];
    state.currentOrder.ref =
      userinfo.username.toString() + currenttime.toString();
    state.statebuffer = "menge";
    state.currentOrder["is_cash"] = true;
  },
  async setCustomerCurrentOrder({ state, dispatch }, customer) {
    state.currentOrder.customer_name = customer.name;
    state.currentOrder.customer_id = customer.id;
    state.currentOrder.customer_address = customer.address;
    state.currentOrder.customer_city = customer.city;
    state.currentOrder.customer_plz = customer.plz;
    state.currentOrder.customer_firma = customer.firma;
    await dispatch("doCalculation");
  },
  async deleteCustomerFromOrder({ state }) {
    state.currentOrder.customer_id = 0;
    state.currentOrder.customer_name = "";
  },

  setOffer({ state }) {
    state.currentOrder.is_offer = !state.currentOrder.is_offer;
  },
  selectOrder({ commit, state, dispatch }, order_ref) {
    if (state.currentOrder.ref === order_ref) {
      return;
    }
    for (let i = 0; i < state.orders.length; i++) {
      if (state.orders[i].ref === order_ref) {
        state.orderselectedindex = i;
        state.currentOrder = state.orders[state.orderselectedindex];
      }
    }
    if (state.currentOrder.lines.length > 0) {
      dispatch("handleClickOnOrderLine", [0, state.currentOrder.lines[0]]);
    }
    state.currentOrder.lines = Array.from(
      Object.keys(state.currentOrder.lines),
      (k) => state.currentOrder.lines[k]
    );
    dispatch("doCalculation");
  },
  selectOrderBySeq({ commit, state, dispatch }, seq) {
    state.orderselectedindex = seq;
    state.currentOrder = state.orders[state.orderselectedindex];
    state.currentOrder.lines = Array.from(
      Object.keys(state.currentOrder.lines),
      (k) => state.currentOrder.lines[k]
    );
    dispatch("doCalculation");
  },

  doAddProductToOrderBarcode({ commit, state, dispatch }, item) {
    dispatch("doAddProductToOrderList", item);
  },
  doSelectProductToOrder({ commit, state, dispatch }, item) {
    state.currentProductKg = {};
    // Kiểm tra đang ở trong trạng thái show popup price info hay không
    if (!state.showPriceInfo) {
      if (item.uom === "kg") {
        state.currentProductKg = { ...item };
        state.currentProductKg.origin_sale_price =
          state.currentProductKg.sale_price;
        state.productWeightPrice =
          Number(state.currentProductKg.sale_price) * 100;

        // Kiểm tra integer number
        if (!Number.isInteger(state.productWeightPrice)) {
          state.productWeightPrice = state.productWeightPrice.toFixed();
        }

        globalStore.showPopup("productKg");
        setTimeout(() => {
          document.querySelector(".input-weight-elm").focus();
        }, 200);
      } else if (item.name.indexOf("Div.") !== -1 && item.uom !== "kg") {
        state.currentProductDiv = { ...item };
        state.productDivMenge = state.currentProductDiv.quantity;
        state.productDivPrice =
          Number(state.currentProductDiv.sale_price) * 100;
        globalStore.showPopup("productDiv");
        setTimeout(() => {
          document.querySelector(".input-product-div-price").focus();
        }, 200);
      } else {
        dispatch("doAddProductToOrderList", item);
      }
    } else {
      item.userid = state.currentOrder.userid;
      dispatch("setDataCurrentProductPopupPriceInfo", item);
      globalStore.showPopup("priceInfo");
      globalStore.socket.emit(
        "openPriceInfoCS",
        JSON.stringify(state.currentProductShowPopupInfo)
      );
    }

    dispatch("checkRefreshApp");
  },
  doSelectProductToOrderRetoure({ commit, state, dispatch }, item) {
    dispatch("doAddProductToOrderListRetoure", item);
  },
  doSelectProductToOrderGeschenk({ commit, state, dispatch }, item) {
    let itemtoadd = { ...item };
    itemtoadd["line_geschenk"] = true;
    dispatch("doAddProductToOrderListKg", itemtoadd);
    setTimeout(() => {
      dispatch("handleClickOnOrderLine", [
        parseInt(state.selectedIndex) + 1,
        state.currentOrder.lines[parseInt(state.selectedIndex) + 1],
      ]);
      itemtoadd["line_geschenk"] = false;
    }, 20);
  },
  searchProductByAN({ commit, state, dispatch }, artikel_nummer) {
    for (let i = 0; i < state.allProduct.length; i++) {
      const prod = state.allProduct[i];
      if (prod["artikel_nummer"] === artikel_nummer) {
        return prod;
      }
    }
    return {};
  },
  async doAddProductToOrderList({ commit, state, dispatch }, item) {
    state.currentOrderline = { ...item };
    state.selectedIndex = 0;
    let checkDuplicate = 0;
    for (let i = 0; i < state.currentOrder.lines.length; i++) {
      const element = state.currentOrder.lines[i];
      if (
        state.currentOrderline.id === element.id &&
        state.currentOrderline.uom === element.uom &&
        element.disable_selected !== true &&
        element["line_geschenk"] !== true &&
        state.currentOrderline.name.indexOf("Div.") !== 0 &&
        state.currentOrderline.barcode.toString().substring(0, 7) !== "2400120"
      ) {
        dispatch("handleClickOnOrderLine", [i, element]);
        let oldquantity = element.quantity;
        element.quantity = parseFloat(element.quantity) + 1;
        if (element.quantity !== 1) {
          if (
            !element["lineDiscountPercentage"] ||
            typeof element.lineDiscountPercentage == "undefined"
          ) {
            element.line_discount = parseFloat(
              (element.line_discount / oldquantity) * element.quantity
            ).toFixed(2);
          }
        }
        checkDuplicate = 1;
        //highlight first order in list
        setTimeout(() => {
          let rowProduct = document.getElementsByClassName(
            "single-row-product-order"
          );
          if (rowProduct.length > 0) {
            for (let row of rowProduct) {
              row.classList.remove("isClicked");
            }
            rowProduct[i].classList.add("isClicked");

            // scroll đến dòng được highlight
            let offsetTopRow = rowProduct[i].offsetTop;
            let productListBody = document.querySelector(".product-list-body");
            let productListHeader = document.querySelector(
              ".product-list-header"
            );
            let heightHeader = productListHeader.getBoundingClientRect().height;
            productListBody.scrollTop = offsetTopRow - heightHeader;
          }
        }, 10);
      }
    }
    if (checkDuplicate === 0) {
      state.currentOrder.lines.unshift(state.currentOrderline);
      state.currentOrderline.quantity = 1;
      state.currentOrderline.linePriceTotal = false;
      //highlight first order in list
      setTimeout(() => {
        let rowProduct = document.getElementsByClassName(
          "single-row-product-order"
        );
        if (rowProduct.length > 0) {
          for (let row of rowProduct) {
            row.classList.remove("isClicked");
          }
          rowProduct[0].classList.add("isClicked");

          // scroll đến dòng được highlight
          let offsetTopRow = rowProduct[0].offsetTop;
          let productListBody = document.querySelector(".product-list-body");
          let productListHeader = document.querySelector(
            ".product-list-header"
          );
          let heightHeader = productListHeader.getBoundingClientRect().height;
          productListBody.scrollTop = offsetTopRow - heightHeader;
        }
      }, 10);
      await dispatch("doCalculation");
      if (typeof state.currentOrderline["geschenk"] !== "undefined") {
        if (state.currentOrderline["geschenk"].toString().length > 3) {
          let searchProductByAN = await dispatch(
            "searchProductByAN",
            state.currentOrderline["geschenk"].toString()
          );
          searchProductByAN["line_geschenk"] = true;
          await dispatch("doSelectProductToOrderGeschenk", searchProductByAN);
        }
      }
    }

    // Xử lý màu của sản phẩm là disable pay
    let rowProduct = document.getElementsByClassName(
      "single-row-product-order"
    );
    for (let j = 0; j < state.currentOrder.lines.length; j++) {
      if (state.currentOrder.lines[j].disable_pay == true) {
        setTimeout(() => {
          if (rowProduct.length > 0) {
            for (let k = 0; k < rowProduct.length; k++) {
              if (j == k) {
                rowProduct[k].classList.add("disableMakePayment");
              }
            }
          }
        }, 10);
      } else {
        setTimeout(() => {
          if (rowProduct.length > 0) {
            for (let l = 0; l < rowProduct.length; l++) {
              if (j == l) {
                rowProduct[l].classList.remove("disableMakePayment");
              }
            }
          }
        }, 10);
      }
    }
    //reset scroll state of list product order box
    state.currentProudctScrollInListOrder = 0;
    // document.getElementsByClassName('product-list-body')[0].scrollTop = 0;
    await dispatch("handleChangeInputState", "menge");
    await dispatch("doCalculation");
  },
  async doAddProductToOrderListRetoure({ commit, state, dispatch }, item) {
    state.currentOrderline = { ...item };
    state.selectedIndex = 0;
    let checkDuplicate = 0;
    for (let i = 0; i < state.currentOrder.lines.length; i++) {
      const element = state.currentOrder.lines[i];
      if (
        state.currentOrderline.id === element.id &&
        state.currentOrderline.uom === element.uom &&
        element.disable_selected !== true &&
        element["line_geschenk"] !== true &&
        state.currentOrderline.name.indexOf("Div.") !== 0 &&
        state.currentOrderline.barcode.toString().substring(0, 7) !== "2400120"
      ) {
        dispatch("handleClickOnOrderLine", [i, element]);
        let oldquantity = element.quantity;
        element.quantity = parseFloat(element.quantity) + 1;
        if (element.quantity !== 1) {
          if (
            !element["lineDiscountPercentage"] ||
            typeof element.lineDiscountPercentage == "undefined"
          ) {
            element.line_discount = parseFloat(
              (element.line_discount / oldquantity) * element.quantity
            ).toFixed(2);
          }
        }
        checkDuplicate = 1;
        //highlight first order in list
        setTimeout(() => {
          let rowProduct = document.getElementsByClassName(
            "single-row-product-order"
          );
          if (rowProduct.length > 0) {
            for (let row of rowProduct) {
              row.classList.remove("isClicked");
            }
            rowProduct[i].classList.add("isClicked");

            // scroll đến dòng được highlight
            let offsetTopRow = rowProduct[i].offsetTop;
            let productListBody = document.querySelector(".product-list-body");
            let productListHeader = document.querySelector(
              ".product-list-header"
            );
            let heightHeader = productListHeader.getBoundingClientRect().height;
            productListBody.scrollTop = offsetTopRow - heightHeader;
          }
        }, 10);
      }
    }
    if (checkDuplicate === 0) {
      state.currentOrder.lines.unshift(state.currentOrderline);
      state.currentOrderline.quantity = 1;
      state.currentOrderline.linePriceTotal = false;
      //highlight first order in list
      setTimeout(() => {
        let rowProduct = document.getElementsByClassName(
          "single-row-product-order"
        );
        if (rowProduct.length > 0) {
          for (let row of rowProduct) {
            row.classList.remove("isClicked");
          }
          rowProduct[0].classList.add("isClicked");

          // scroll đến dòng được highlight
          let offsetTopRow = rowProduct[0].offsetTop;
          let productListBody = document.querySelector(".product-list-body");
          let productListHeader = document.querySelector(
            ".product-list-header"
          );
          let heightHeader = productListHeader.getBoundingClientRect().height;
          productListBody.scrollTop = offsetTopRow - heightHeader;
        }
      }, 10);
      await dispatch("doCalculation");
    }

    // Xử lý màu của sản phẩm là disable pay
    let rowProduct = document.getElementsByClassName(
      "single-row-product-order"
    );
    for (let j = 0; j < state.currentOrder.lines.length; j++) {
      if (state.currentOrder.lines[j].disable_pay == true) {
        setTimeout(() => {
          if (rowProduct.length > 0) {
            for (let k = 0; k < rowProduct.length; k++) {
              if (j == k) {
                rowProduct[k].classList.add("disableMakePayment");
              }
            }
          }
        }, 10);
      } else {
        setTimeout(() => {
          if (rowProduct.length > 0) {
            for (let l = 0; l < rowProduct.length; l++) {
              if (j == l) {
                rowProduct[l].classList.remove("disableMakePayment");
              }
            }
          }
        }, 10);
      }
    }
    //reset scroll state of list product order box
    state.currentProudctScrollInListOrder = 0;
    // document.getElementsByClassName('product-list-body')[0].scrollTop = 0;
    await dispatch("handleChangeInputState", "menge");
    await dispatch("doCalculation");
  },
  async doAddProductToOrderListBarcode({ commit, state, dispatch }, item) {
    if (item.name.indexOf("Div.") !== -1 && item.uom !== "kg") {
      state.currentProductDiv = { ...item };
      state.productDivMenge = state.currentProductDiv.quantity;
      state.productDivPrice = Number(state.currentProductDiv.sale_price) * 100;
      globalStore.showPopup("productDiv");
      setTimeout(() => {
        document.querySelector(".input-product-div-price").focus();
      }, 200);
      return;
    }
    if (state.showPriceInfo) {
      item.userid = state.currentOrder.userid;
      dispatch("setDataCurrentProductPopupPriceInfo", item);
      globalStore.showPopup("priceInfo");
      globalStore.socket.emit(
        "openPriceInfoCS",
        JSON.stringify(state.currentProductShowPopupInfo)
      );
      return;
    }
    state.currentOrderline = { ...item };
    state.selectedIndex = 0;
    let checkDuplicate = 0;
    for (let i = 0; i < state.currentOrder.lines.length; i++) {
      const element = state.currentOrder.lines[i];
      if (
        state.currentOrderline.id === element.id &&
        state.currentOrderline.uom === element.uom &&
        element.disable_selected !== true &&
        state.currentOrderline.name.indexOf("Div.") != 0 &&
        state.currentOrderline.barcode.toString().substring(0, 7) != "2400120"
      ) {
        dispatch("handleClickOnOrderLine", [i, element]);
        let oldquantity = element.quantity;
        element.quantity = parseFloat(element.quantity) + 1;
        if (element.quantity !== 1) {
          if (
            !element.lineDiscountPercentage ||
            typeof element.lineDiscountPercentage == "undefined"
          ) {
            element.line_discount = parseFloat(
              (element.line_discount / oldquantity) * element.quantity
            ).toFixed(2);
          }
        }
        checkDuplicate = 1;
        //highlight first order in list
        setTimeout(() => {
          let rowProduct = document.getElementsByClassName(
            "single-row-product-order"
          );
          if (rowProduct.length > 0) {
            for (let row of rowProduct) {
              row.classList.remove("isClicked");
            }
            rowProduct[i].classList.add("isClicked");

            // scroll đến dòng được highlight
            let offsetTopRow = rowProduct[i].offsetTop;
            let productListBody = document.querySelector(".product-list-body");
            let productListHeader = document.querySelector(
              ".product-list-header"
            );
            let heightHeader = productListHeader.getBoundingClientRect().height;
            productListBody.scrollTop = offsetTopRow - heightHeader;
          }
        }, 10);
      }
    }
    if (checkDuplicate === 0) {
      state.currentOrder.lines.unshift(state.currentOrderline);
      state.currentOrderline.quantity = 1;
      state.currentOrderline.linePriceTotal = false;
      //highlight first order in list
      setTimeout(() => {
        let rowProduct = document.getElementsByClassName(
          "single-row-product-order"
        );
        if (rowProduct.length > 0) {
          for (let row of rowProduct) {
            row.classList.remove("isClicked");
          }
          rowProduct[0].classList.add("isClicked");

          // scroll đến dòng được highlight
          let offsetTopRow = rowProduct[0].offsetTop;
          let productListBody = document.querySelector(".product-list-body");
          let productListHeader = document.querySelector(
            ".product-list-header"
          );
          let heightHeader = productListHeader.getBoundingClientRect().height;
          productListBody.scrollTop = offsetTopRow - heightHeader;
        }
      }, 10);
      await dispatch("doCalculation");
      if (typeof state.currentOrderline["geschenk"] !== "undefined") {
        if (state.currentOrderline["geschenk"].toString().length > 3) {
          let searchProductByAN = await dispatch(
            "searchProductByAN",
            state.currentOrderline["geschenk"].toString()
          );
          searchProductByAN["line_geschenk"] = true;
          await dispatch("doSelectProductToOrderGeschenk", searchProductByAN);
        }
      }
    }

    //reset scroll state of list product order box
    state.currentProudctScrollInListOrder = 0;
    // document.getElementsByClassName('product-list-body')[0].scrollTop = 0;
    dispatch("handleChangeInputState", "menge");
    dispatch("doCalculation");
  },
  doAddProductToOrderListKg({ commit, state, dispatch }, item) {
    state.currentOrderline = { ...item };
    state.selectedIndex = 0;
    state.currentOrderline.linePriceTotal = false;
    // Kiểm tra nếu mà giá nhập vô nhỏ hơn giá gốc thì mình phải tính cái đó là discount chứ không được sửa giá gốc
    if (
      state.currentOrderline.sale_price <
      state.currentOrderline.origin_sale_price
    ) {
      state.currentOrderline.discount_value = parseFloat(
        state.currentOrderline.quantity *
          (state.currentOrderline.origin_sale_price -
            state.currentOrderline.sale_price)
      ).toFixed(2);
      state.currentOrderline.line_discount = parseFloat(
        state.currentOrderline.discount_value
      ).toFixed(2);
      state.currentOrderline.sale_price = parseFloat(
        state.currentOrderline.origin_sale_price
      ).toFixed(2);
    }
    state.currentOrder.lines.unshift(state.currentOrderline);
    //highlight first order in list
    setTimeout(() => {
      let rowProduct = document.getElementsByClassName(
        "single-row-product-order"
      );
      if (rowProduct.length > 0) {
        for (let row of rowProduct) {
          row.classList.remove("isClicked");
        }
        rowProduct[0].classList.add("isClicked");

        // scroll đến dòng được highlight
        let offsetTopRow = rowProduct[0].offsetTop;
        let productListBody = document.querySelector(".product-list-body");
        let productListHeader = document.querySelector(".product-list-header");
        let heightHeader = productListHeader.getBoundingClientRect().height;
        productListBody.scrollTop = offsetTopRow - heightHeader;
      }
    }, 10);
    //reset scroll state of list product order box
    state.currentProudctScrollInListOrder = 0;
    // document.getElementsByClassName('product-list-body')[0].scrollTop = 0;
    dispatch("handleChangeInputState", "menge");
    dispatch("doCalculation");
  },
  handleConfirmPopupProductKg({ commit, state, dispatch }) {
    state.currentProductKg.sale_price = Number(state.productWeightPrice) / 100;
    state.currentProductKg.quantity = Number(state.productWeight) / 1000;
    dispatch("doAddProductToOrderListKg", state.currentProductKg);
  },
  handleConfirmPopupProductDiv({ commit, state, dispatch }) {
    state.currentProductDiv.sale_price = Number(state.productDivPrice) / 100;
    state.currentProductDiv.quantity = state.productDivMenge;
    dispatch("doAddProductToOrderListKg", state.currentProductDiv);
  },
  handleChangePackung({ commit, state, dispatch }) {
    let currentLine = state.currentOrder.lines[state.selectedIndex];
    if (currentLine.disable_selected) {
      return;
    }

    if (currentLine.packung_price === 0 || currentLine["line_geschenk"]) {
      globalStore.showPopup("warningUmpackung");
      return;
    }

    currentLine.sale_price = currentLine.packung_price;
    currentLine.uom = "Umpackung";
    if (parseInt(currentLine["geschenk"]) > 100) {
      let geschenk_line = state.currentOrder.lines[state.selectedIndex - 1];
      if (!geschenk_line["name"].toLowerCase().includes("pfand")) {
        geschenk_line.uom = "Umpackung";
        geschenk_line.sale_price = geschenk_line.packung_price;
      }
    }
    dispatch("doCalculation");
  },
  handleChangeKarton({ commit, state, dispatch }) {
    let currentLine = state.currentOrder.lines[state.selectedIndex];
    if (currentLine.disable_selected) {
      return;
    }

    if (currentLine.karton_price === 0 || currentLine["line_geschenk"]) {
      globalStore.showPopup("warningKarton");
      return;
    }

    currentLine.sale_price = currentLine.karton_price;
    currentLine.uom = "Karton";
    if (parseInt(currentLine["geschenk"]) > 100) {
      let geschenk_line = state.currentOrder.lines[state.selectedIndex - 1];
      if (!geschenk_line["name"].toLowerCase().includes("pfand")) {
        geschenk_line.uom = "Karton";
        geschenk_line.sale_price = geschenk_line.karton_price;
      }
    }
    dispatch("doCalculation");
  },
  async doCalculation({ commit, state, dispatch }) {
    // Chặn tính toán lại khi chưa có dữ liệu gì
    if (typeof state.currentOrder.lines === "undefined") {
      return;
    }
    if (state.currentOrder.lines.length > 0) {
      dispatch("updateDetailLine");
    }
    let userinfo = JSON.parse(
      localStorage.getItem(globalStore.cookieNameLoginInfo)
    ).value[0];
    // Kiểm tra nếu giá trị socket thay đổi thì mới bắn order lên socket và tính toán
    if (
      state.currentOrderSocket !== JSON.stringify(state.currentOrder) &&
      typeof state.currentOrder !== "undefined"
    ) {
      state.currentOrder.userid = userinfo.userid;
      await commit("calTotalValue");
      state.currentOrderSocket = JSON.stringify(state.currentOrder);
      await globalStore.sendSocket(
        state.currentOrderSocket,
        userinfo.username.toString()
      );
    }

    state.isActiveQuantityPopup = false;
  },
  checkCanMakeOrder({ state }) {
    let selectedline = state.currentOrder.lines[state.selectedIndex];
    // Kiểm tra discount có lớn hơn 30% hay không
    if (
      parseFloat(selectedline.discount_value) /
        parseFloat(selectedline.subtotal) >
      0.3
    ) {
      return true;
    }
    // Kiểm tra xem có giá min price hay không và giá cuối có nhỏ hơn min price hay không.
    // return selectedline.min_price > 0 && parseFloat(selectedline.sale_price) < parseFloat(selectedline.min_price);
    let check = false;
    if (selectedline.sale_price <= 0) {
      return false;
    }

    if (selectedline.uom === "Umpackung") {
      if (selectedline.packung_min_price > 0) {
        if (
          selectedline.packung_price > 0 &&
          selectedline.packung_price > selectedline.packung_min_price
        ) {
          if (
            parseFloat(selectedline.subPrice) <
            parseFloat(selectedline.packung_min_price)
          ) {
            // check min price
            check = true;
          }
          if (
            parseFloat(selectedline.sale_price) <
            parseFloat(selectedline.packung_min_price)
          ) {
            check = true;
          } else {
            if (
              parseFloat(selectedline.sale_price) <
              parseFloat(selectedline.sale_price) * 0.85
            ) {
              check = true;
            }
          }
        } else {
          if (
            parseFloat(selectedline.sale_price) <
            parseFloat(selectedline.sale_price) * 0.85
          ) {
            check = true;
          }
        }
      } else {
        if (
          parseFloat(selectedline.sale_price) <
          parseFloat(selectedline.sale_price) * 0.85
        ) {
          check = true;
        }
      }
    }
    if (selectedline.uom === "Karton") {
      if (selectedline.karton_min_price > 0) {
        if (
          selectedline.karton_price > 0 &&
          selectedline.karton_price > selectedline.karton_min_price
        ) {
          if (
            parseFloat(selectedline.subPrice) <
            parseFloat(selectedline.karton_min_price)
          ) {
            // check min price
            check = true;
          }
          if (
            parseFloat(selectedline.sale_price) <
            parseFloat(selectedline.karton_min_price)
          ) {
            check = true;
          } else {
            if (
              parseFloat(selectedline.sale_price) <
              parseFloat(selectedline.sale_price) * 0.85
            ) {
              check = true;
            }
          }
        } else {
          if (
            parseFloat(selectedline.sale_price) <
            parseFloat(selectedline.sale_price) * 0.85
          ) {
            check = true;
          }
        }
      } else {
        if (
          parseFloat(selectedline.sale_price) <
          parseFloat(selectedline.sale_price) * 0.85
        ) {
          check = true;
        }
      }
    } else {
      if (selectedline.min_price > 0) {
        if (
          selectedline.origin_sale_price > 0 &&
          selectedline.origin_sale_price > selectedline.min_price
        ) {
          if (
            parseFloat(selectedline.subPrice) <
            parseFloat(selectedline.min_price)
          ) {
            // check min price
            check = true;
          }
          if (
            parseFloat(selectedline.sale_price) <
            parseFloat(selectedline.min_price)
          ) {
            check = true;
          } else {
            if (
              parseFloat(selectedline.sale_price) <
              parseFloat(selectedline.sale_price) * 0.85
            ) {
              check = true;
            }
          }
        } else {
          if (
            parseFloat(selectedline.sale_price) <
            parseFloat(selectedline.sale_price) * 0.85
          ) {
            check = true;
          }
        }
      } else {
        if (
          parseFloat(selectedline.sale_price) <
          parseFloat(selectedline.sale_price) * 0.85
        ) {
          check = true;
        }
      }
    }
    return check;
  },
  handleClickOnOrderLine({ commit, state, dispatch }, [index, product]) {
    // nếu mà dòng này bị disable_selected thì không được click vào nữa
    if (product.disable_selected === true) {
      return;
    }
    state.currentOrderline = product; // Dữ liệu orderline
    state.selectedIndex = index; // Index của orderline
    state.orderlineIsHighlight = index; // Index của orderline -> Biến này làm trùng với biến của anh Khoa vì sợ ảnh hưởng tới code của anh Khoa
    state.buffer = "";
    state.statebuffer = "menge";

    state.linePriceTotal = product.linePriceTotal;
    state.lineDiscountPercentage = product.lineDiscountPercentage;

    //highlight first order in list
    setTimeout(() => {
      let rowProduct = document.getElementsByClassName(
        "single-row-product-order"
      );
      if (rowProduct.length > 0) {
        for (let row of rowProduct) {
          row.classList.remove("isClicked");
        }
        rowProduct[index].classList.add("isClicked");
      }
    }, 50);
  },
  changePriceState({ commit, state, dispatch }, pricestate) {
    let selectedline = state.currentOrder.lines[state.selectedIndex];
    if (typeof selectedline === "undefined") {
      globalStore.showPopup("warningNoProduct");
      return;
    }
    if (pricestate === "total") {
      selectedline.linePriceTotal = true;
      state.linePriceTotal = true;
    }
    if (pricestate === "single") {
      selectedline.linePriceTotal = false;
      state.linePriceTotal = false;
    }
    selectedline.line_discount = 0;
  },
  changeRabattState({ commit, state, dispatch }, rabattstate) {
    let selectedline = state.currentOrder.lines[state.selectedIndex];
    if (typeof selectedline === "undefined") {
      return;
    }
    if (rabattstate === "percent") {
      state.lineDiscountPercentage = true;
      selectedline.lineDiscountPercentage = true;
    }
    if (rabattstate === "money") {
      state.lineDiscountPercentage = false;
      selectedline.lineDiscountPercentage = false;
    }
    selectedline.line_discount = 0;
    selectedline.linePriceTotal = false;
  },
  handleChangeInputState({ commit, state, dispatch }, currentstate) {
    if (typeof state.currentOrder.lines === "undefined") {
      return;
    }
    let selectedline = state.currentOrder.lines[state.selectedIndex];
    if (currentstate === "menge") {
      state.linePriceTotal = false;
      if (typeof selectedline !== "undefined") {
        selectedline.linePriceTotal = false;
      }
    }
    state.statebuffer = currentstate;
    state.buffer = "";

    dispatch("doCalculation");
  },
  async ClickPayment({ commit, state, dispatch }, value) {
    // Kiểm tra order có đang là order trống không - Nếu đang là order trống thì không xử lý khi click vào payment
    let checkblankorder = await dispatch("checkblankorder");
    if (checkblankorder === true) {
      globalStore.showPopup("warningNoProduct");
      return;
    }
    if (value === "cash") {
      state.currentOrder.is_cash = true;
    }
    if (value === "card") {
      state.currentOrder.is_cash = false;
    }
    dispatch("doCalculation");
    for (let i = 0; i < state.currentOrder.lines.length; i++) {
      if (
        globalStore.checkValid(state.currentOrder.lines[i].disable_selected) &&
        state.currentOrder.lines[i].disable_pay === true
      ) {
        router.push({ path: "/payment", query: { type: value } });
      } else if (
        !globalStore.checkValid(state.currentOrder.lines[i].disable_selected) &&
        state.currentOrder.lines[i].disable_pay === true
      ) {
        globalStore.showPopup("requirePasswordDiscountLine");
        return;
      }
    }
    router.push({ path: "/payment", query: { type: value } });
  },
  resetLine({ rootState, state, dispatch }, status) {
    if (typeof state.currentOrderline.id === "undefined") {
      return;
    }
    let selectedline = state.currentOrder.lines[state.selectedIndex];
    let allproduct = rootState.product.allProduct;
    if (status === "all") {
      selectedline.quantity = 1;
      for (let i = 0; i < allproduct.length; i++) {
        let selectproduct = allproduct[i];
        if (selectproduct.id === selectedline.id) {
          if (selectedline.name.indexOf("Div.") !== -1) {
            selectedline.sale_price = 0;
          } else {
            selectedline.sale_price = selectproduct.sale_price;
          }
          selectedline.uom = selectproduct.uom;
          break;
        }
      }
      selectedline.discount_value = 0;
      selectedline.line_discount = 0;
      selectedline.total_price = parseFloat(
        selectedline.sale_price * selectedline.quantity
      ).toFixed(2);
    }
    if (status === "price") {
      for (let i = 0; i < allproduct.length; i++) {
        let selectproduct = allproduct[i];
        if (selectproduct.id === selectedline.id) {
          selectedline.sale_price = selectproduct.sale_price;
          selectedline.uom = selectproduct.uom;
          break;
        }
      }
      selectedline.discount_value = 0;
      selectedline.line_discount = 0;
    }
    if (status === "quantity") {
      let discountline = parseFloat(selectedline.line_discount);
      let discount_value = parseFloat(selectedline.discount_value);
      if (discountline === discount_value) {
        selectedline.discount_value = discount_value / selectedline.quantity;
        selectedline.line_discount = discountline / selectedline.quantity;
      }
      selectedline.quantity = 1;
    }
    if (status === "discount") {
      selectedline.discount_value = 0;
      selectedline.line_discount = 0;
    }
    selectedline.linePriceTotal = false;
    state.buffer = "";
    state.statebuffer = "menge";
    state.linePriceTotal = false;
    dispatch("doCalculation");
  },
  confirmPriceRabatt({ rootState, state, dispatch }) {
    let selectedline = state.currentOrder.lines[state.selectedIndex];
    if (state.statebuffer === "preis") {
      if (selectedline.linePriceTotal) {
        selectedline.sale_price = parseFloat(
          selectedline.origin_sale_price
        ).toFixed(2);
        selectedline.total_price = (parseFloat(state.buffer) / 100).toFixed(2);
      } else {
        // Kiểm tra nếu mà giá nhập vô nhỏ hơn giá gốc thì mình phải tính cái đó là discount chứ không được sửa giá gốc
        let origin_price = parseFloat(selectedline.origin_sale_price);
        let new_price = parseFloat(state.buffer) / 100;
        if (new_price < origin_price) {
          selectedline.discount_value = parseFloat(
            selectedline.quantity * (origin_price - new_price)
          ).toFixed(2);
          selectedline.line_discount = parseFloat(
            selectedline.discount_value
          ).toFixed(2);
          selectedline.sale_price = parseFloat(
            selectedline.origin_sale_price
          ).toFixed(2);
        } else {
          selectedline.sale_price = new_price.toFixed(2);
        }
      }
    }
    if (state.statebuffer === "rabatt") {
      if (state.lineDiscountPercentage) {
        if (state.buffer > 80) {
          state.buffer = "80";
        }
        selectedline.line_discount = state.buffer;
      } else {
        if (
          parseFloat(state.buffer) / 100 >
          parseFloat(selectedline.subtotal) * 0.8
        ) {
          state.buffer = (selectedline.subtotal * 0.8).toFixed(2).toString();
          selectedline.line_discount = parseFloat(state.buffer).toFixed(2);
        } else {
          selectedline.line_discount = (parseFloat(state.buffer) / 100).toFixed(
            2
          );
        }
      }
      selectedline.lineDiscountPercentage = state.lineDiscountPercentage;
    }
    dispatch("doCalculation");
  },
  setUomCurrentLine({ rootState, state, dispatch }, value) {
    let selectedline = state.currentOrder.lines[state.selectedIndex];
    selectedline.uom = value;
  },
  calLineDiscountWithBuffer({ state }, bufferType) {
    const selectedline = state.currentOrder.lines[state.selectedIndex];

    if (bufferType >= 1) {
      if (
        state.lineDiscountPercentage === false ||
        typeof state.lineDiscountPercentage == "undefined"
      ) {
        if (bufferType.toString().length > 1) {
          let sotruoc = bufferType
            .toString()
            .substring(0, bufferType.length - 1);
          selectedline.line_discount = parseFloat(
            (selectedline.line_discount / sotruoc) * bufferType
          ).toFixed(2);
        } else {
          selectedline.line_discount = parseFloat(
            selectedline.line_discount * bufferType
          ).toFixed(2);
          selectedline.line_discount =
            selectedline.line_discount / selectedline.quantity;
        }
      }
    }
  },
  handleClickNumberBoard({ rootState, state, dispatch }, value) {
    let selectedline = state.currentOrder.lines[state.selectedIndex];

    if (typeof selectedline === "undefined") {
      return;
    }
    if (typeof state.currentOrderline.id === "undefined") {
      return;
    }
    if (
      typeof state.currentOrderline != "undefined" &&
      state.currentOrderline.disable_selected === true
    ) {
      return;
    }
    dispatch("calBufferValue", value);
    if (value === "back") {
      globalStore.showPopup("confirmClear");
      return;
    }
    if (value === "clear") {
      state.buffer = "";
      return;
    }
    if (state.statebuffer === "menge") {
      let isWarning = false;

      // Không cho số lẻ
      if (state.buffer.indexOf(".") !== -1 && selectedline.uom !== "kg") {
        isWarning = true;
        state.buffer = state.buffer.substring(0, state.buffer.length - 1);
        globalStore.showPopup("warningComma");
      }
      // if (selectedline.quantity !== 1 && state.buffer == 1) {
      //   selectedline.line_discount = 0;
      // }
      // if (selectedline.uom === 'kg') {
      //   let tile = state.buffer / 1;
      //   let discountgoc = selectedline.line_discount;
      //   if (state.buffer.toString().length > 1) {
      //     let sotruoc = state.buffer.toString().substring(0, state.buffer.length - 1);
      //     selectedline.line_discount = parseFloat(selectedline.line_discount / sotruoc * state.buffer).toFixed(2);
      //   }else{
      //     selectedline.line_discount = parseFloat(selectedline.line_discount * state.buffer).toFixed(2);
      //     selectedline.line_discount = selectedline.line_discount / selectedline.quantity
      //   }
      // }
      if (state.selectedIndex > 0 && !isWarning) {
        state.isActiveQuantityPopup = true;
        state.buffer = "";
        globalStore.showPopup("increaseQuantity");
        return;
      }

      dispatch("calLineDiscountWithBuffer", state.buffer);
      selectedline.quantity = state.buffer;
    }
    if (state.statebuffer === "rabatt") {
      //Chỗ này chỉ dùng cho retoure
      if (selectedline.quantity < 0) {
        selectedline.line_discount = -value;
      }
    }
    if (state.statebuffer === "preis") {
      //Chỗ này chỉ dùng cho retoure
      if (selectedline.quantity < 0) {
        selectedline.sale_price = value;
      }
    }
    dispatch("doCalculation");
  },
  handleClickNumQuantity({ state, dispatch }, value) {
    dispatch("calQuantityBufferValue", value);

    if (value === "clear") {
      state.quantityBuffer = "";
      return;
    }
  },
  confirmIncreaseQuantity({ state, dispatch }) {
    const selectedline = state.currentOrder.lines[state.selectedIndex];

    if (state.quantityBuffer === "") return;

    dispatch("calLineDiscountWithBuffer", state.quantityBuffer);
    selectedline.quantity = state.quantityBuffer;

    if (selectedline.uom === "kg") {
      selectedline.quantity = Number(selectedline.quantity) / 1000;
    }

    dispatch("doCalculation");
    state.quantityBuffer = "";
  },
  setPreisValue({ state, dispatch }, value) {
    // value của Preis gồm 1 value, nếu muốn âm phải gán value âm trước
    let selectedline = state.currentOrder.lines[state.selectedIndex];
    selectedline.sale_price = value;
    dispatch("doCalculation");
  },
  setMengeValue({ state, dispatch }, value) {
    // value của Menge gồm 1 value, nếu muốn âm phải gán value âm trước
    let selectedline = state.currentOrder.lines[state.selectedIndex];
    selectedline.quantity = value;
    dispatch("doCalculation");
  },
  setRabattValue({ state, dispatch }, value) {
    // value của discount gồm 1 object chứa 3 thông tin {line_discount,discount_value,linediscountpercentage}
    let selectedline = state.currentOrder.lines[state.selectedIndex];
    selectedline.line_discount = value.line_discount;
    selectedline.discount_value = value.discount_value;
    selectedline.lineDiscountPercentage = value.linediscountpercentage;
    dispatch("doCalculation");
  },
  handleClickNumberBoardBarcodethitca({ rootState, state, dispatch }, value) {
    let selectedline = state.currentOrder.lines[state.selectedIndex];
    if (state.statebuffer === "rabatt") {
      // Chỗ này còn dùng cho cả scan barcode thịt cá
      selectedline.line_discount = -value;
    }
    if (state.statebuffer === "preis") {
      // Chỗ này còn dùng cho cả scan barcode thịt cá
      selectedline.sale_price = value;
    }
    if (state.statebuffer === "menge") {
      // Chỗ này còn dùng cho cả scan barcode thịt cá
      selectedline.quantity = value;
    }
    dispatch("doCalculation");
  },
  handleClickNumberBoardKg({ rootState, state, dispatch }, value) {
    if (state.isInputWeightFocus) {
      document.querySelector(".input-weight-elm").focus();
      dispatch("calInputKgValue", ["weight", value]);
      if (value === "clear") {
        state.productWeight = "";
        return;
      }
    }
    if (state.isInputWeightPriceFocus) {
      document.querySelector(".input-weight-product-elm").focus();
      dispatch("calInputKgValue", ["weightPrice", value]);
      if (value === "clear") {
        state.productWeightPrice = "";
        return;
      }
    }
  },
  handleClickNumberBoardDiv({ rootState, state, dispatch }, value) {
    if (state.isInputDivMenge) {
      document.querySelector(".input-product-div-menge").focus();
      dispatch("calInputProductDivValue", ["menge", value]);
      if (value === "clear") {
        state.productDivMenge = "";
        return;
      }
    }
    if (state.isInputDivAmount) {
      document.querySelector(".input-product-div-price").focus();
      dispatch("calInputProductDivValue", ["price", value]);
      if (value === "clear") {
        state.productDivPrice = "";
        return;
      }
    }
  },
  calInputProductDivValue({ state }, [model, newvalue]) {
    if (model === "menge") {
      state.productDivMenge = state.productDivMenge.toString();
      // Không cho 2 dấu chấm
      if (state.productDivMenge.indexOf(".") !== -1 && newvalue === ".") {
        return;
      }
      // Sau dấu phẩy thì không được nhiều hơn 2 ký tự
      if (
        state.productDivMenge.charAt(state.productDivMenge.length - 2) === "."
      ) {
        return;
      }
      // Nếu mới vô bấm 00 thì nó thành 0
      if (state.productDivMenge === "" || state.productDivMenge === "0") {
        if (newvalue === "00") {
          state.productDivMenge = "0";
          return;
        }
      }
      // Nếu số 0 ở đầu tiên thì đừng thêm số 0 vô
      if (state.productDivMenge === "0") {
        state.productDivMenge = newvalue;
        return;
      }
      if (state.productDivMenge === "" || state.productDivMenge === "0") {
        if (newvalue === ".") {
          state.productDivMenge = "0.";
          return;
        }
      }
      if (state.productDivMenge.charAt(0) === ".") {
        state.productDivMenge = "0.";
      }
      state.productDivMenge = state.productDivMenge + newvalue;
    }
    if (model === "price") {
      state.productDivPrice = state.productDivPrice.toString();
      // Không cho 2 dấu chấm
      if (state.productDivPrice.indexOf(".") !== -1 && newvalue === ".") {
        return;
      }
      // Sau dấu phẩy thì không được nhiều hơn 2 ký tự
      if (
        state.productDivPrice.charAt(state.productDivPrice.length - 3) === "."
      ) {
        return;
      }
      // Nếu mới vô bấm 00 thì nó thành 0
      if (state.productDivPrice === "" || state.productDivPrice === "0") {
        if (newvalue === "00") {
          state.productDivPrice = "0";
          return;
        }
      }
      // Nếu số 0 ở đầu tiên thì đừng thêm số 0 vô
      if (state.productDivPrice === "0") {
        state.productDivPrice = newvalue;
        return;
      }
      if (state.productDivPrice === "" || state.productDivPrice === "0") {
        if (newvalue === ".") {
          state.productDivPrice = "0.";
          return;
        }
      }
      if (state.productDivPrice.charAt(0) === ".") {
        state.productDivPrice = "0.";
      }
      state.productDivPrice = state.productDivPrice + newvalue;
    }
  },
  calInputKgValue({ state }, [model, newvalue]) {
    // let dataInput = '';
    if (model === "weight") {
      // Không cho 2 dấu chấm
      if (state.productWeight.indexOf(".") !== -1 && newvalue === ".") {
        return;
      }
      // Sau dấu phẩy thì không được nhiều hơn 3 ký tự
      if (state.productWeight.charAt(state.productWeight.length - 4) === ".") {
        return;
      }
      // Nếu mới vô bấm 00 thì nó thành 0
      if (state.productWeight === "" || state.productWeight === "0") {
        if (newvalue === "00") {
          state.productWeight = "0";
          return;
        }
      }
      // Nếu số 0 ở đầu tiên thì đừng thêm số 0 vô
      if (state.productWeight === "0") {
        state.productWeight = newvalue;
        return;
      }
      if (state.productWeight === "" || state.productWeight === "0") {
        if (newvalue === ".") {
          state.productWeight = "0.";
          return;
        }
      }
      if (state.productWeight.charAt(0) === ".") {
        state.productWeight = "0.";
      }
      state.productWeight = state.productWeight + newvalue;
    }
    if (model === "weightPrice") {
      state.productWeightPrice = state.productWeightPrice.toString();
      // Không cho 2 dấu chấm
      if (state.productWeightPrice.indexOf(".") !== -1 && newvalue === ".") {
        return;
      }
      // Sau dấu phẩy thì không được nhiều hơn 2 ký tự
      if (
        state.productWeightPrice.charAt(state.productWeightPrice.length - 3) ===
        "."
      ) {
        return;
      }
      // Nếu mới vô bấm 00 thì nó thành 0
      if (state.productWeightPrice === "" || state.productWeightPrice === "0") {
        if (newvalue === "00") {
          state.productWeightPrice = "0";
          return;
        }
      }
      // Nếu số 0 ở đầu tiên thì đừng thêm số 0 vô
      if (state.productWeightPrice === "0") {
        state.productWeightPrice = newvalue;
        return;
      }
      if (state.productWeightPrice === "" || state.productWeightPrice === "0") {
        if (newvalue === ".") {
          state.productWeightPrice = "0.";
          return;
        }
      }
      if (state.productWeightPrice.charAt(0) === ".") {
        state.productWeightPrice = "0.";
      }
      state.productWeightPrice = state.productWeightPrice + newvalue;
    }
  },
  clearStateProductKg({ commit, state, dispatch }) {
    state.productWeightPrice = "";
    state.productWeight = "";
    state.isInputWeightFocus = "";
    state.isInputWeightPriceFocus = "";
  },
  calBufferValue({ state }, newvalue) {
    // Không cho 2 dấu chấm
    if (state.buffer.indexOf(".") !== -1 && newvalue === ".") {
      return;
    }
    // Bấm back là xóa lui, tới số cuối cùng là chuyển thành 1
    if (newvalue === "back") {
      if (state.buffer.length === 1) {
        state.buffer = "0";
        return;
      }
      state.buffer = state.buffer.slice(0, -1);
      return;
    }
    // Nếu số 0 ở đầu tiên thì đừng thêm số 0 vô
    if (state.buffer === "0") {
      state.buffer = newvalue;
      return;
    }
    if (state.buffer === "" || state.buffer === "0") {
      if (newvalue === ".") {
        state.buffer = "0.";
        return;
      }
    }
    if (state.buffer.charAt(0) === ".") {
      state.buffer = "0.";
    }
    // Sau dấu phẩy thì không được nhiều hơn 2 ký tự
    if (state.buffer.charAt(state.buffer.length - 3) === ".") {
      let selectedline = state.currentOrder.lines[state.selectedIndex];
      if (selectedline.uom === "kg" && state.statebuffer === "menge") {
        state.buffer = state.buffer + newvalue;
        return;
      }
      return;
    }
    if (state.buffer.charAt(state.buffer.length - 4) === ".") {
      return;
    }

    state.buffer = state.buffer + newvalue;
  },
  calQuantityBufferValue({ state }, value) {
    // TODO: Logic is similar to calInputKgValue

    // Không cho 2 dấu chấm
    if (state.quantityBuffer.indexOf(".") !== -1 && value === ".") {
      return;
    }

    // Sau dấu phẩy thì không được nhiều hơn 3 ký tự
    if (state.quantityBuffer.charAt(state.quantityBuffer.length - 4) === ".") {
      return;
    }

    // Nếu mới vô bấm 00 thì nó thành 0
    if (state.quantityBuffer === "" || state.quantityBuffer === "0") {
      if (value === "00") {
        state.quantityBuffer = "0";
        return;
      }
    }

    // Nếu số 0 ở đầu tiên thì đừng thêm số 0 vô
    if (state.quantityBuffer === "0") {
      state.quantityBuffer = value;
      return;
    }
    if (state.quantityBuffer === "" || state.quantityBuffer === "0") {
      if (value === ".") {
        state.quantityBuffer = "0.";
        return;
      }
    }
    if (state.quantityBuffer.charAt(0) === ".") {
      state.quantityBuffer = "0.";
    }

    state.quantityBuffer = state.quantityBuffer + value;
  },
  async updateDetailLine({ state, dispatch }) {
    if (typeof state.selectedIndex === "undefined") {
      state.selectedIndex = 0;
    }
    let selectedline = state.currentOrder.lines[state.selectedIndex];
    // let response = await axios.post(`${globalStore.baseUrl}product/beginchangedate`, postBody);
    // let val = JSON.parse(response.data.result);
    // let datebegin = val.value;
    //
    // respon = await axios.post(`${globalStore.baseUrl}product/endchangedate`, postBody);
    // val = JSON.parse(respon.data.result);
    // let dateend = val.value;
    //
    // if (this.$moment(new Date()) >= this.$moment(datebegin) &&
    //   this.$moment(new Date()) <= this.$moment(dateend)) {
    //   if(selectedline.mwst === '7'){
    //     selectedline.mwst = '5';
    //   }
    //   if(selectedline.mwst === '19'){
    //     selectedline.mwst = '16';
    //   }
    // }
    selectedline.subtotal = parseFloat(
      parseFloat(selectedline.quantity) * parseFloat(selectedline.sale_price)
    ).toFixed(2);
    if (
      !selectedline.linePriceTotal ||
      typeof !selectedline.linePriceTotal === "undefined"
    ) {
      if (
        !selectedline.lineDiscountPercentage ||
        typeof selectedline.lineDiscountPercentage === "undefined"
      ) {
        selectedline.line_discount = isNaN(selectedline.line_discount)
          ? 0
          : selectedline.line_discount;
        selectedline.discount_value = parseFloat(
          selectedline.line_discount
        ).toFixed(2);
      } else {
        selectedline.discount_value =
          (selectedline.subtotal * selectedline.line_discount) / 100;
      }

      selectedline.subPrice = parseFloat(
        selectedline.sale_price -
          selectedline.discount_value / selectedline.quantity
      ).toFixed(2); // Price after discount

      selectedline.total_price = parseFloat(
        selectedline.subtotal - selectedline.discount_value
      ).toFixed(2);
      selectedline.total_price_without_tax = parseFloat(
        (selectedline.total_price * 100) / (100 + parseFloat(selectedline.mwst))
      ).toFixed(2);
      if (selectedline.mwst === "7" || selectedline.mwst === "5") {
        selectedline.mwst7 = parseFloat(
          selectedline.total_price - selectedline.total_price_without_tax
        ).toFixed(2);
      }
      if (selectedline.mwst === "19" || selectedline.mwst === "16") {
        selectedline.mwst19 = parseFloat(
          selectedline.total_price - selectedline.total_price_without_tax
        ).toFixed(2);
      }
      selectedline.total_mwst =
        parseFloat(selectedline.mwst7 || 0) +
        parseFloat(selectedline.mwst19 || 0);
    } else {
      state.lineDiscountPercentage = false;
      selectedline.lineDiscountPercentage = false;
      selectedline.line_discount = Math.abs(
        selectedline.total_price - selectedline.subtotal
      ).toFixed(2);
      selectedline.discount_value = Math.abs(
        selectedline.total_price - selectedline.subtotal
      ).toFixed(2);
      selectedline.total_price_without_tax = parseFloat(
        (selectedline.total_price * 100) / (100 + parseFloat(selectedline.mwst))
      ).toFixed(2);
      if (selectedline.mwst === "7" || selectedline.mwst === "5") {
        selectedline.mwst7 = parseFloat(
          selectedline.total_price - selectedline.total_price_without_tax
        ).toFixed(2);
      }
      if (selectedline.mwst === "19" || selectedline.mwst === "16") {
        selectedline.mwst19 = parseFloat(
          selectedline.total_price - selectedline.total_price_without_tax
        ).toFixed(2);
      }
      selectedline.total_mwst =
        parseFloat(selectedline.mwst7 || 0) +
        parseFloat(selectedline.mwst19 || 0);
    }
    dispatch("checkCanMakeOrder").then((retget) => {
      if (retget) {
        selectedline.disable_pay = true;
        let rowProduct = document.getElementsByClassName(
          "single-row-product-order isClicked"
        );
        if (typeof rowProduct[0] !== "undefined") {
          rowProduct[0].classList.add("disableMakePayment");
        }
      } else {
        selectedline.disable_pay = false;
        let rowProduct = document.getElementsByClassName(
          "single-row-product-order isClicked"
        );
        if (typeof rowProduct[0] !== "undefined") {
          rowProduct[0].classList.remove("disableMakePayment");
        }
      }
    });
    if (parseInt(selectedline["geschenk"]) > 100) {
      if (state.selectedIndex > 0) {
        let selectedline_geschenk =
          state.currentOrder.lines[state.selectedIndex - 1];
        if (selectedline_geschenk["name"].toLowerCase().includes("pfand") > 0) {
          if (selectedline["uom"] === "Karton") {
            selectedline_geschenk["quantity"] =
              parseFloat(selectedline["karton_menge"]) *
              parseFloat(selectedline["quantity"]);
          } else if (selectedline["uom"] === "Umpackung") {
            selectedline_geschenk["quantity"] =
              parseFloat(selectedline["packung_menge"]) *
              parseFloat(selectedline["quantity"]);
          } else {
            selectedline_geschenk["quantity"] = selectedline["quantity"];
          }
          if (selectedline["quantity"] < 0) {
            selectedline_geschenk["quantity"] = 0;
          }
          state.selectedIndex = state.selectedIndex - 1;
          dispatch("updateDetailLine");
          state.selectedIndex = state.selectedIndex + 1;
        } else {
          selectedline_geschenk["quantity"] = selectedline["quantity"];
          if (selectedline["quantity"] < 0) {
            selectedline_geschenk["quantity"] = 0;
          }
          state.selectedIndex -= 1;
          dispatch("updateDetailLine");
          state.selectedIndex += 1;
        }
      }
    }
  },
  handleDataScrollProductBox({ commit, state, dispatch, rootState }, [value]) {
    let pageCount = 1;

    pageCount = Math.ceil(state.allProduct.length / state.productsPerPage);
    if (!rootState.category.isSearching) {
      pageCount = Math.ceil(state.allProduct.length / state.productsPerPage);
    } else {
      pageCount = Math.ceil(
        state.productsFiltering.length / state.productsPerPage
      );
    }

    function showNext() {
      state.pagerProductList =
        state.pagerProductList == pageCount ? 1 : state.pagerProductList + 1;
      showPage(state.pagerProductList);
    }

    function showPrev() {
      state.pagerProductList =
        state.pagerProductList == 1 ? pageCount : state.pagerProductList - 1;
      showPage(state.pagerProductList);
    }

    function showPage(page) {
      let pageIndex = (page - 1) * state.productsPerPage;

      if (!rootState.category.isSearching) {
        state.allProductLazyLoad = state.allProduct.slice(
          pageIndex,
          pageIndex + state.productsPerPage
        );
      } else {
        state.allProductLazyLoad = state.productsFiltering.slice(
          pageIndex,
          pageIndex + state.productsPerPage
        );
      }

      setTimeout(() => {
        let list = document.querySelector(".category-child-items");
        let items = list.querySelectorAll(".single-product-item");
        globalStore.handleAlternateColorButton(list, items, 6);
      }, 80);
    }

    switch (value) {
      case "down":
        showNext();
        break;
      case "up":
        showPrev();
    }
  },
  handleDataScrollProductOrderList({ commit, state, dispatch }, [value]) {
    switch (value) {
      case "down":
        state.totalProductOrder = state.currentOrder.lines.length;
        if (state.totalProductOrder > state.productInview) {
          let tempProductLeft =
            state.totalProductOrder - state.currentProudctScrollInListOrder;
          if (tempProductLeft > state.productInview) {
            state.currentProudctScrollInListOrder += state.productInview;
          } else {
            state.currentProudctScrollInListOrder += tempProductLeft - 1;
          }
          globalStore.scrollToElement(
            "product-list-body",
            "single-row-product-order",
            state.currentProudctScrollInListOrder
          );
        } else {
          state.currentProudctScrollInListOrder = 0;
        }
        break;
      case "up":
        if (
          state.currentProudctScrollInListOrder ===
          state.totalProductOrder - 1
        ) {
          state.currentProudctScrollInListOrder -= state.productInview * 2 - 1;
        } else {
          state.currentProudctScrollInListOrder -= state.productInview;
        }
        if (state.currentProudctScrollInListOrder <= 0) {
          state.currentProudctScrollInListOrder = 0;
        }
        globalStore.scrollToElement(
          "product-list-body",
          "single-row-product-order",
          state.currentProudctScrollInListOrder
        );
        break;
    }
  },
  handleUpDownOrderlineSelected({ commit, state, dispatch }, [value]) {
    let rowOrderLine = document.querySelectorAll(".single-row-product-order");
    if (rowOrderLine.length > 0) {
      let totalLines = rowOrderLine.length;

      rowOrderLine.forEach((el, index) => {
        if (el.classList.contains("isClicked")) {
          dispatch("setValueIndexOrderlineHighlight", index);
        }
      });

      if (value === "down") {
        if (state.orderlineIsHighlight < totalLines - 1) {
          for (
            let i = state.orderlineIsHighlight + 1;
            i < state.currentOrder.lines.length;
            i++
          ) {
            if (!state.currentOrder.lines[i].disable_selected) {
              dispatch("setValueIndexOrderlineHighlight", i);
              break;
            }
          }
        }
      }
      if (value === "up") {
        if (state.orderlineIsHighlight > 0) {
          for (let i = state.orderlineIsHighlight - 1; i >= 0; i--) {
            if (!state.currentOrder.lines[i].disable_selected) {
              dispatch("setValueIndexOrderlineHighlight", i);
              break;
            }
          }
        }
        if (state.orderlineIsHighlight < 0) {
          dispatch("setValueIndexOrderlineHighlight", 0);
        }
      }
      rowOrderLine.forEach((elRow, indexRow) => {
        elRow.classList.remove("isClicked");
      });
      rowOrderLine[state.orderlineIsHighlight].classList.add("isClicked");
      if (state.orderlineIsHighlight < 5) {
        globalStore.scrollToElement(
          "product-list-body",
          "single-row-product-order",
          0
        );
      } else {
        globalStore.scrollToElement(
          "product-list-body",
          "single-row-product-order",
          state.orderlineIsHighlight
        );
      }

      dispatch("handleClickOnOrderLine", [
        state.orderlineIsHighlight,
        state.currentOrder.lines[state.orderlineIsHighlight],
      ]);
    }
  },
  handleProductStorno({ rootState, commit, state, dispatch }) {
    if (
      state.currentOrder.lines[state.selectedIndex].disable_selected === true
    ) {
      globalStore.showPopup("warningNotStorno");
      return;
    }
    let itemtoadd = { ...state.currentOrder.lines[state.selectedIndex] };
    if (itemtoadd.quantity === 0) {
      rootState.popup.warningContent = globalStore.$i18n.t(
        "Quantity should more than 0"
      );
      globalStore.showPopup("showWarning");
      return;
    }
    itemtoadd.quantity = -state.currentOrder.lines[state.selectedIndex]
      .quantity;
    // Tạo attribute disable_selected để đánh dấu line này không thể thay đổi
    state.currentOrder.lines[state.selectedIndex].disable_selected = true;
    itemtoadd.disable_selected = true;
    state.currentOrder.lines.splice(state.selectedIndex, 0, itemtoadd);
    dispatch("updateDetailLine");
    // state.currentOrder.lines.unshift(itemtoadd);
    state.selectedIndex = 0;
    dispatch("updateDetailLine");
    for (let i = 0; i < state.currentOrder.lines.length; i++) {
      if (state.currentOrder.lines[i].disable_selected !== true) {
        state.selectedIndex = i;
        dispatch("handleClickOnOrderLine", [i, state.currentOrder.lines[i]]);
        dispatch("setValueIndexOrderlineHighlight", state.selectedIndex);
        break;
      }
    }
    dispatch("doCalculation");
  },
  handleProductStornoAll({ commit, state, dispatch }) {
    let temp_store = [];
    let orderlength = state.currentOrder.lines.length;
    for (let i = 0; i < orderlength; i++) {
      if (state.currentOrder.lines[i].disable_selected !== true) {
        let item = { ...state.currentOrder.lines[i] };
        temp_store.push(item);
      }
    }
    for (let i = 0; i < temp_store.length; i++) {
      let item = { ...temp_store[i] };
      item.quantity = -temp_store[i].quantity;
      // Nếu là discount tiền thì phải làm âm số, còn nếu discount % thì để nó tự nhân ra âm
      if (parseFloat(item.line_discount) === parseFloat(item.discount_value)) {
        item.line_discount = -temp_store[i].line_discount;
        item.discount_value = -temp_store[i].discount_value;
      }
      state.currentOrder.lines.unshift(item);
      state.selectedIndex = 0;
      dispatch("updateDetailLine");
      dispatch("doCalculation");
    }
  },
  handleStateFocusInputProductKg(
    { commit, state, dispatch },
    [value, boolean]
  ) {
    if (value === "inputWeight" && boolean === true) {
      state.isInputWeightFocus = true;
      state.isInputWeightPriceFocus = false;
    }
    if (value === "inputWeightPrice" && boolean === true) {
      state.isInputWeightFocus = false;
      state.isInputWeightPriceFocus = true;
      // state.productWeightPrice = '';
    }
  },
  handleStateFocusInputProductDiv(
    { commit, state, dispatch },
    [value, boolean]
  ) {
    if (value === "inputDivMenge" && boolean === true) {
      state.isInputDivMenge = true;
      state.isInputDivAmount = false;
    }
    if (value === "inputDivPrice" && boolean === true) {
      state.isInputDivMenge = false;
      state.isInputDivAmount = true;
    }
  },
  resetValueInputProductDivWhenFocus({ commit, state, dispatch }) {
    state.productDivMenge = "";
  },
  handleShowHideKeyboardSearchProduct({ commit, state, dispatch }, value) {
    state.isShowKeyBoardSearchProduct = value;
  },
  setValueIndexOrderlineHighlight({ commit, state, dispatch }, value) {
    state.orderlineIsHighlight = value;
  },
  handleBeginAmountInFirstLoad({ commit, state, dispatch }, value) {
    state.isBeginAmountStarted = value;
  },
  setDataCurrentProductPopupPriceInfo({ commit, state, dispatch }, value) {
    state.currentProductShowPopupInfo = value;
  },
  resetPagerProductList({ commit, state, dispatch }) {
    state.pagerProductList = 1;
  },
  checkRefreshApp({ commit, state, dispatch }) {
    state.isActiveRefreshApp = true;
    for (let i = 0; i < state.orders.length; i++) {
      if (state.orders[i].lines.length > 0) {
        state.isActiveRefreshApp = false;
        return;
      }
    }
  },
  checkblankorder() {
    if (typeof state.orderselectedindex === "undefined") {
      state.orderselectedindex = 0;
    }
    let orderlines = state.currentOrder.lines;
    return typeof orderlines === "undefined" || orderlines.length === 0;
  },
  check_have_blankorder({ state }) {
    let checkcheck = false;
    for (let i = 0; i < state.orders.length; i++) {
      if (
        typeof state.orders[i].lines === "undefined" ||
        state.orders[i].lines === 0
      ) {
        checkcheck = true;
      }
    }
    return checkcheck;
  },
};

const mutations = {
  resetNewOrder(state) {
    let userinfo = JSON.parse(
      localStorage.getItem(globalStore.cookieNameLoginInfo)
    ).value[0];
    let currenttime = new Date().getTime();
    state.newOrder = {
      ref: userinfo.username.toString() + currenttime.toString(),
      beleg: "",
      userid: userinfo.userid,
      username: userinfo.username.toString(),
      is_cash: true,
      lines: [],
      total_discount: 0,
      total_discount_in_amount: 0,
      total_with_discount: 0,
      total_without_discount: 0,
      orderdiscountlines: 0,

      discount_type: "amount",
      total_discount_mwst7: 0,
      total_discount_mwst19: 0,

      total_give: 0,
      total_give_display: 0,
      paymentType: "cash",
      return_back: 0,

      return_state: "",
      return_ref: "",
      return_beleg: "",

      total7: 0,
      total19: 0,

      mwst: 0,
      mwst7: 0,
      mwst19: 0,
      netto: 0,
      subtotal: 0,

      lineDiscountPercentage: false,
      is_offer: false,
      is_packlist: true,

      orderdate: "",
      ordertime: "",
      session_id: "",

      customer_name: "",
      customer_id: 0,

      retoure_grund: "",
    };
  },
  resetCurrentOrder(state) {
    let userinfo = JSON.parse(
      localStorage.getItem(globalStore.cookieNameLoginInfo)
    ).value[0];
    state.currentOrder = {
      beleg: "",
      userid: userinfo.userid,
      username: userinfo.username.toString(),
      is_cash: true,
      lines: [],
      total_discount: 0,
      total_discount_in_amount: 0,
      total_with_discount: 0,
      total_without_discount: 0,
      orderdiscountlines: 0,

      discount_type: "amount",
      total_discount_mwst7: 0,
      total_discount_mwst19: 0,

      total_give: 0,
      total_give_display: 0,
      paymentType: "cash",
      return_back: 0,

      return_state: "",
      return_ref: "",
      return_beleg: "",

      total7: 0,
      total19: 0,

      mwst: 0,
      mwst7: 0,
      mwst19: 0,
      netto: 0,
      subtotal: 0,

      lineDiscountPercentage: false,
      is_offer: false,
      is_packlist: true,

      orderdate: "",
      ordertime: "",
      session_id: "",

      customer_name: "",
      customer_id: 0,

      retoure_grund: "",
    };
  },
  setDataProduct(state, allProduct) {
    state.allProduct = allProduct;
  },
  setDataProductLazyLoad(state, products) {
    state.allProductLazyLoad = [];

    let length = 0;

    if (products.length > state.productsPerPage) {
      length = state.productsPerPage;
    } else {
      length = products.length;
    }

    for (let i = 0; i < length; i++) {
      state.allProductLazyLoad.push(products[i]);
    }
  },
  setDataScrollProductBox(state) {
    if (state.allProduct.length > 0) {
      state.totalProduct = state.allProduct.length;
    } else {
      state.totalProduct = 0;
    }
    state.currentProductItemScrollTo = 0;
  },
  setDataScrollProductOrderList(state) {
    state.totalProductOrder = 0;
    state.currentProudctScrollInListOrder = 0;
  },
  calTotalValue(state) {
    let subtotal = 0;
    let mwst7 = 0;
    let mwst19 = 0;
    let total7 = 0;
    let total19 = 0;
    let orderdiscountlines = 0;
    for (let i = 0; i < state.currentOrder.lines.length; i++) {
      subtotal = parseFloat(
        parseFloat(subtotal) +
          parseFloat(state.currentOrder.lines[i].total_price || 0)
      ).toFixed(2);
      mwst7 = parseFloat(
        parseFloat(mwst7) + parseFloat(state.currentOrder.lines[i].mwst7 || 0)
      ).toFixed(2);
      mwst19 = parseFloat(
        parseFloat(mwst19) + parseFloat(state.currentOrder.lines[i].mwst19 || 0)
      ).toFixed(2);
      if (
        state.currentOrder.lines[i].mwst === "7" ||
        state.currentOrder.lines[i].mwst === "5"
      ) {
        total7 = parseFloat(
          parseFloat(total7) +
            parseFloat(state.currentOrder.lines[i].total_price || 0)
        ).toFixed(2);
      }
      if (
        state.currentOrder.lines[i].mwst === "19" ||
        state.currentOrder.lines[i].mwst === "16"
      ) {
        total19 = parseFloat(
          parseFloat(total19) +
            parseFloat(state.currentOrder.lines[i].total_price || 0)
        ).toFixed(2);
      }
      orderdiscountlines = parseFloat(
        parseFloat(orderdiscountlines) +
          parseFloat(state.currentOrder.lines[i].discount_value || 0)
      ).toFixed(2);
    }
    state.currentOrder.subtotal = subtotal;
    state.currentOrder.mwst7 = mwst7;
    state.currentOrder.mwst19 = mwst19;
    state.currentOrder.mwst = parseFloat(
      parseFloat(mwst7) + parseFloat(mwst19)
    ).toFixed(2);
    state.currentOrder.total_with_discount =
      state.currentOrder.subtotal - state.currentOrder.total_discount_in_amount;
    state.currentOrder.total_without_discount =
      parseFloat(state.currentOrder.subtotal) +
      parseFloat(state.currentOrder.orderdiscountlines);
    state.currentOrder.netto =
      state.currentOrder.total_with_discount - state.currentOrder.mwst;
    state.currentOrder.total7 = total7;
    state.currentOrder.total19 = total19;
    state.currentOrder.orderdiscountlines = orderdiscountlines;
  },
  initSelectedindex(state) {
    state.selectedindex = 0;
  },
  changeStatePopupPriceInfo(state) {
    state.showPriceInfo = !state.showPriceInfo;
  },
  setRetoureGrund(state, val) {
    state.orders[state.orderselectedindex].retoure_grund = val;
  },
  handleStateClickFocusInputProductKg(state) {
    state.productWeightPrice = "";
  },
  setDefaultDiscountType(state, value) {
    state.lineDiscountPercentage = value;
  },

  setCheckRefreshLogin(state, value) {
    state.checkRefreshLogin = value;
  },

  setProductsFiltering(state, value) {
    state.productsFiltering = value;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
