import { globalStore } from '@/global/global.js';
import axios from "axios";

const state = { // data
  isHomeReady: false,
  enable_retoure: true,
};

const getters = { // computed
};

const actions = { // methods

};

const mutations = { // handle response from actions to update state
  setHomeReady(state, value) {
    state.isHomeReady = value;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
