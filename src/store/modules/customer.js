import { globalStore } from '@/global/global.js';
import axios from 'axios'
import router from '../../router';
import VueCookies from 'vue-cookies';
import commonService from '@/services/common.service.js';

const state = { // data
    allCustomer: [],
    customerSelected: {},
    customerOrderSelected: {},
    warningcontent: '',
    loginFalse: false,
    barcodePassword: '',
}

const getters = { // computed
}

const actions = { // methods
    loginByBarcode({commit, dispatch, state, rootState}, barcode) {
        const postBody = {
            "params": {
                "barcode": barcode,
                "db": globalStore.db
            }
        };
        axios.post(`${globalStore.baseUrl}user/loginbarcode`, postBody).then(response => {
            this.loadingLogin = false;
            this.dataUser = JSON.parse(response.data.result);
            this.dataUser.value[0].logintime = Math.round(new Date()/1000);
            let socketask = {'username': this.dataUser.value[0].username, 'logintime': this.dataUser.value[0].logintime}
            if (this.dataUser.info === 'Successful') {
                let cookieLogin = VueCookies.get(globalStore.cookieNameLoginInfo);
                if (!commonService.checkValid(cookieLogin)) {
                    VueCookies.set(globalStore.cookieNameLoginInfo, JSON.stringify(this.dataUser), -1);
                } else {
                    VueCookies.remove(globalStore.cookieNameLoginInfo);
                    VueCookies.set(globalStore.cookieNameLoginInfo, JSON.stringify(this.dataUser), -1);
                }
                commonService.storeToLocalStorage(globalStore.cookieNameLoginInfo, this.dataUser);

                if(this.dataUser.value[0].cws_group === "SALE"){
                    globalStore.socket.emit('login', this.dataUser.value[0].cws_group);
                }
                globalStore.socket.emit('ask_socket_login', socketask);
                if(this.dataUser.value[0].cws_group === "MANAGER"){
                    router.push({path: '/bericht'});
                }else{
                    router.push({path: '/home'});
                }
            } else {
                state.loginFalse = true;
                router.push({path: '/'});
            }
        });
    },
    setCustomerSelectedById({commit, state, dispatch}, customerId) {
        var custById = state.customerList.filter(customer => {
            return customer.id = customerId;
        });
        state.customerSelected = custById[0];
    },
    confirmBarcodePassword({commit, dispatch, state, rootState}, barcode) {
        const postBody = {
            "params": {
                "barcode": barcode,
                "db": globalStore.db
            }
        };
        let retval = axios.post(`${globalStore.baseUrl}user/loginbarcode`, postBody).then(response => {
            this.loadingLogin = false;
            this.dataUser = JSON.parse(response.data.result);
            if (this.dataUser.info === 'Successful') {
                if(this.dataUser.value[0].cws_group === 'MANAGER'){
                    this.barcodePassword = this.dataUser.value[0].passcode;
                }
            }
            return this.barcodePassword;
        });
        return retval;
    },
}

const mutations = { // handle response from actions to update state
    setDataCustomer(state, allCustomer) {
        state.allCustomer = allCustomer;
    },
}

export default {
  state,
  getters,
  actions,
  mutations
}
