import {
  globalStore
} from '@/global/global.js';
import axios from 'axios';

const state = { // data
  reprintData: {},
  customerSelected: {},
};

const getters = { // computed

};

const actions = { // methods
  save_reprint_order({commit, state, dispatch}, order){
    state.reprintData = order;
    if(order.customer_id !== 0){
      commit('set_customer_by_id', order);
    }
  },
  save_reprint_orderlines({commit, state, dispatch}, orderlines){
    state.reprintData.lines = orderlines;
  }
};

const mutations = { // handle response from actions to update state
  set_customer_by_id(state, order){
    if(order.customer_id !== 0){
      const datacustomer = {
        "params": {
          "customer_id": order.customer_id
        }
      };
      axios.post(`${globalStore.baseUrl}customer/get_by_id`, datacustomer).then(response => {
        let ret = JSON.parse(response.data.result);
        let retorder = JSON.parse(ret.value.replace(/\bNaN\b/g, 'null'));
        state.reprintData.customer_firma = retorder[0].firma;
        state.reprintData.customer_address = retorder[0].address;
        state.reprintData.customer_plz = retorder[0].plz;
        state.reprintData.customer_city = retorder[0].city;
      });
    }
  },
};

export default {
  state,
  getters,
  actions,
  mutations
};
