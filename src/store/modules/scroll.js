import { globalStore } from '@/global/global.js';

const state = { // data
    currentPoint: 0
}

const getters = { // computed
}

const actions = { // methods
    updateCurrentPoint({commit, state, dispatch}, currentPointParam) {
        state.currentPoint = currentPointParam;
    }
}

const mutations = { // handle response from actions to update state
    initCurrentPoint(state) {
        state.currentPoint = 0;
    }
}

export default {
  state,
  getters,
  actions,
  mutations
}
