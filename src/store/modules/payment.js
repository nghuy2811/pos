import { globalStore } from "@/global/global.js";
import router from "../../router";
import axios from "axios";
const getters = {};
const state = {
  // data
  buffer: "",
  buffertemp: "",
  selected_place: "gegeben", // Chỗ này dùng để check vị trí đang click
  discount_type: "amount",
  clickMoneyCount: 0,
  countPayment: 0,
};

const actions = {
  // methods
  async handleClickNumpad({ commit, rootState, dispatch }, value) {
    if (rootState.product.currentOrder.paymentType == "card") return;

    if (value === "passen") {
      dispatch("handleClickPassen");
      return;
    }
    await commit("setBuffer", value);
    dispatch("doCalculationPayment");
  },
  async getCurrentOrderPayment({ state, rootState, dispatch }) {
    if (typeof rootState.product.currentOrder.lines === "undefined") {
      return;
    }
    rootState.product.currentOrder.mwst7 = 0;
    rootState.product.currentOrder.mwst19 = 0;
    rootState.product.currentOrder.total7 = 0;
    rootState.product.currentOrder.total19 = 0;
    rootState.product.currentOrder.total_without_discount = 0;
    rootState.product.currentOrder.lines.map((e) => {
      rootState.product.currentOrder.mwst7 = (
        parseFloat(rootState.product.currentOrder.mwst7) +
        parseFloat(e.mwst7 || 0)
      ).toFixed(2);
      rootState.product.currentOrder.mwst19 = (
        parseFloat(rootState.product.currentOrder.mwst19) +
        parseFloat(e.mwst19 || 0)
      ).toFixed(2);
      rootState.product.currentOrder.total_without_discount = (
        parseFloat(rootState.product.currentOrder.total_without_discount) +
        parseFloat(e.total_price)
      ).toFixed(2);
      if (e.mwst === "19" || e.mwst === "16") {
        rootState.product.currentOrder.total19 = (
          parseFloat(rootState.product.currentOrder.total19) +
          parseFloat(e.total_price)
        ).toFixed(2);
      }
      if (e.mwst === "7" || e.mwst === "5") {
        rootState.product.currentOrder.total7 = (
          parseFloat(rootState.product.currentOrder.total7) +
          parseFloat(e.total_price)
        ).toFixed(2);
      }
    });
    rootState.product.currentOrder.total_discount = 0;
    rootState.product.currentOrder.total_discount_in_amount = 0;
    rootState.product.currentOrder.discount_type = "amount";
    rootState.product.currentOrder.total_with_discount =
      rootState.product.currentOrder.total_without_discount;
    rootState.product.currentOrder.return_back = 0;
    rootState.product.currentOrder.paymentType = globalStore.$route.query.type;
    rootState.product.currentOrder.total_give_display = (
      parseFloat(rootState.product.currentOrder.total_with_discount) * 100
    ).toFixed(0);
    rootState.product.currentOrder.total_give = parseFloat(
      rootState.product.currentOrder.total_with_discount
    );
    dispatch("doCalculation");
  },
  handleChangeDiscountType({ state, rootState, dispatch }, value) {
    rootState.product.currentOrder.discount_type = value;
    dispatch("doCalculationPayment");
  },
  confirmDiscountTotalValue({ rootState, commit, dispatch }) {
    let discount = {
      total_discount: 0,
      total_discount_in_amount: 0,
    };
    let discountamount = 0;
    state.buffertemp = state.buffertemp === "" ? 0 : state.buffertemp;
    if (rootState.product.currentOrder.discount_type === "percent") {
      discountamount = (
        (parseFloat(rootState.product.currentOrder.subtotal) *
          parseFloat(state.buffertemp)) /
        100
      ).toFixed(2);
      discount = {
        total_discount: state.buffertemp,
        total_discount_in_amount: discountamount,
      };
      dispatch("setDiscountValue", discount);
    }
    if (rootState.product.currentOrder.discount_type === "amount") {
      discountamount = (parseFloat(state.buffertemp) / 100).toFixed(2);
      discount = {
        total_discount: discountamount,
        total_discount_in_amount: discountamount,
      };
      dispatch("setDiscountValue", discount);
    }
    rootState.product.currentOrder.total_give = (
      parseFloat(rootState.product.currentOrder.subtotal) - discountamount
    ).toFixed(2);
    dispatch("calculateValue");
    globalStore.socket.emit(
      "updaterabatt",
      JSON.stringify({
        userid: rootState.product.currentOrder.userid,
        total_discount_in_amount:
          rootState.product.currentOrder.total_discount_in_amount,
      })
    );
  },
  // Khi bấm vô nút passen
  handleClickPassen({ state, rootState, dispatch }) {
    state.selected_place = "gegeben";
    // rootState.product.total_discount = 0;
    // rootState.product.total_discount_in_amount = 0;
    // rootState.product.total_with_discount = rootState.product.total_without_discount;
    rootState.product.currentOrder.total_give =
      rootState.product.currentOrder.subtotal;
    rootState.product.currentOrder.total_give_display = (
      rootState.product.currentOrder.total_give * 100
    ).toFixed(0);
    rootState.product.currentOrder.return_back = 0;
    state.buffer = rootState.product.currentOrder.total_give_display;
    dispatch("doCalculationPayment");
  },
  // Khi bấm vô nút tiền ở bên dưới
  handleClickMoney({ commit, dispatch }, value) {
    dispatch("setMoney", value);
    dispatch("doCalculationPayment");
  },
  // Khi bấm vô nút thanh toán bằng card ở bên dưới
  handleClickCard() {
    globalStore.showPopup("confirmCardPay");
  },
  setDiscountTotal({ commit, dispatch }, value) {
    dispatch("setDiscountValue", value);
    // dispatch('calculateValue');
    dispatch("calculateAfterTotalDiscount");
  },
  // Tính toán lại toàn bộ dữ liệu
  doCalculationPayment({ commit, dispatch }) {
    dispatch("setValue");
    dispatch("calculateValue");
    dispatch("calculateAfterTotalDiscount");
  },
  async checkRequirePassPayment({ rootState }) {
    // Function này sẽ kiểm tra nếu có 1 discount trong mỗi dòng và total discount > 1
    // Check tỉ lệ total discount > 15%

    let localOrder = rootState.product.currentOrder;

    let retValue = false;
    let lines = localOrder.lines;
    let discountlines = false;
    let discounttotals = false;
    for (let i = 0; i < lines.length; i++) {
      if (parseFloat(lines[i].discount_value) !== 0) {
        discountlines = true;
      }
    }
    if (localOrder.total_discount_in_amount > 1) {
      discounttotals = true;
    }
    let acceprate =
      localOrder.total_discount_in_amount / localOrder.total_without_discount;
    if (acceprate > 0.16) {
      globalStore.showPopup("requirePasswordDiscountTotal");
      retValue = true;
    }
    if (discountlines && discounttotals) {
      globalStore.showPopup("requirePasswordDiscountTotal");
      retValue = true;
    }
    return retValue;
  },
  async moveToPrintPage({ dispatch }) {
    let checkpassword = await dispatch("checkRequirePassPayment");
    if (!checkpassword) {
      let response = await axios.get(
        `${globalStore.baseUrl}socket/checkonline`,
        {}
      );
      if (response.data.sucessfull) {
        router.push({ path: "/print" });
      }
    }
  },
  resetTotalGive({ commit, dispatch }) {
    commit("setBuffer", "reset");
    dispatch("doCalculationPayment");
  },
  setDiscountValue({ state, rootState, dispatch }, val) {
    // val là object với 2 giá trị discount số và discount_value
    rootState.product.currentOrder.total_discount = val.total_discount;
    rootState.product.currentOrder.total_discount_in_amount =
      val.total_discount_in_amount;
    rootState.product.currentOrder.total_give = (
      parseFloat(rootState.product.currentOrder.subtotal) -
      parseFloat(val.total_discount_in_amount)
    ).toFixed(2);
    rootState.product.currentOrder.total_give_display = (
      parseFloat(rootState.product.currentOrder.total_give) * 100
    ).toFixed(0);
    rootState.product.currentOrder.total_with_discount =
      rootState.product.currentOrder.subtotal -
      rootState.product.currentOrder.total_discount_in_amount;
    dispatch("doCalculation");
  },
  resetBackBtn({ state, rootState, dispatch }) {
    // val là object với 2 giá trị discount số và discount_value
    rootState.product.currentOrder.total_discount = 0;
    rootState.product.currentOrder.total_discount_in_amount = 0;
    rootState.product.currentOrder.total_give = 0;
    rootState.product.currentOrder.total_give_display = 0;
    rootState.product.currentOrder.total7 = 0;
    rootState.product.currentOrder.total19 = 0;
    dispatch("doCalculation");
  },
  calculateValue({ state, rootState, dispatch }) {
    rootState.product.currentOrder.total_with_discount = (
      parseFloat(rootState.product.currentOrder.subtotal) -
      parseFloat(rootState.product.currentOrder.total_discount_in_amount)
    ).toFixed(2);
    rootState.product.currentOrder.return_back = (
      parseFloat(rootState.product.currentOrder.total_give) -
      parseFloat(rootState.product.currentOrder.total_with_discount)
    ).toFixed(2);
    dispatch("doCalculation");
  },
  setPaymentType({ state, rootState, dispatch }, type) {
    rootState.product.currentOrder.paymentType = type;
    rootState.product.currentOrder.is_cash = type === "cash";
    dispatch("doCalculation");
  },
  setValue({ state, rootState, dispatch }) {
    if (state.selected_place === "gegeben") {
      rootState.product.currentOrder.total_give_display = state.buffer;
      rootState.product.currentOrder.total_give = state.buffer / 100;

      // state.total_give_display = rootState.product.currentOrder.total_give_display;
      // state.total_give = rootState.product.currentOrder.total_give;

      dispatch("doCalculation");
    }
  },
  setMoney({ state, rootState, dispatch }, value) {
    state.selected_place = "gegeben";
    state.buffer = rootState.product.currentOrder.total_give_display || 0;
    if (
      state.clickMoneyCount === 0 ||
      rootState.product.currentOrder.total_with_discount ===
        rootState.product.currentOrder.total_give
    ) {
      state.buffer = parseFloat(value).toString();
    } else {
      state.buffer = parseFloat(
        parseFloat(state.buffer) + parseFloat(value)
      ).toString();
    }
    rootState.product.currentOrder.total_give_display = state.buffer;
    rootState.product.currentOrder.total_give =
      rootState.product.currentOrder.total_give_display / 100;
    rootState.product.currentOrder.return_back =
      rootState.product.currentOrder.total_give -
      rootState.product.currentOrder.total_with_discount;
    state.clickMoneyCount = 1;
    dispatch("doCalculation");
  },
  handleClickNumberBoardDiscountTotal({ state, rootState, dispatch }, val) {
    if (val === "clear") {
      state.buffertemp = "";
      return;
    }
    if (state.buffertemp === "" || state.buffertemp === "0") {
      if (val === ".") {
        state.buffertemp = "0" + val;
        return;
      }
      if (val === "0" || val === "00") {
        state.buffertemp = "0";
        return;
      } else {
        state.buffertemp = "" + val;
        return;
      }
    }
    state.buffertemp += val;
    if (rootState.product.currentOrder.discount_type === "percent") {
      if (state.buffertemp > 70) {
        state.buffertemp = 70;
      }
    }
    if (rootState.product.currentOrder.discount_type === "amount") {
      if (
        state.buffertemp / 100 >
        parseFloat(rootState.product.currentOrder.total_without_discount) * 0.7
      ) {
        state.buffertemp =
          parseFloat(rootState.product.currentOrder.total_without_discount) *
          0.7 *
          100;
      }
    }
  },

  // Chú ý hàm này cần check lại kỹ
  calculateAfterTotalDiscount({ state, rootState }) {
    if (typeof rootState.product.currentOrder === "undefined") {
      return;
    }
    if (
      rootState.product.currentOrder.total7 > 0 ||
      rootState.product.currentOrder.total19 > 0
    ) {
      let rate7 =
        parseFloat(rootState.product.currentOrder.total7 || 0) /
        parseFloat(
          parseFloat(rootState.product.currentOrder.total7 || 0) +
            parseFloat(rootState.product.currentOrder.total19 || 0)
        );
      let rate19 = 1 - rate7;
      let discount7 =
        rate7 * rootState.product.currentOrder.total_discount_in_amount;
      let discount19 =
        rate19 * rootState.product.currentOrder.total_discount_in_amount;
      let mwst_discount7 = 0;
      let mwst_discount19 = 0;
      if (
        rootState.product.currentOrder.lines[0].mwst === "5" ||
        rootState.product.currentOrder.lines[0].mwst === "16"
      ) {
        mwst_discount7 = discount7 - discount7 / 1.05;
        mwst_discount19 = discount19 - discount19 / 1.16;
      }
      if (
        rootState.product.currentOrder.lines[0].mwst === "7" ||
        rootState.product.currentOrder.lines[0].mwst === "19"
      ) {
        mwst_discount7 = discount7 - discount7 / 1.07;
        mwst_discount19 = discount19 - discount19 / 1.19;
      }
      state.mwst_discount7 = parseFloat(mwst_discount7).toFixed(2);
      state.mwst_discount19 = parseFloat(mwst_discount19).toFixed(2);
    }
    rootState.product.currentOrder.netto =
      rootState.product.currentOrder.total_with_discount -
      rootState.product.currentOrder.mwst;
  },
};

const mutations = {
  // handle response from actions to update state

  setBuffer(state, val) {
    if (val === "reset") {
      state.buffer = "0";
      return;
    }
    if (val === "delete") {
      if (state.buffer.length > 1) {
        state.buffer = state.buffer.slice(0, -1);
      } else {
        state.buffer = 0;
      }
      return;
    }
    if (state.buffer === "" || state.buffer === "0") {
      if (val === ".") {
        state.buffer = "0" + val;
        return;
      }
      if (val === "0" || val === "00") {
        state.buffer = "0";
        return;
      } else {
        state.buffer = "" + val;
        return;
      }
    }
    if (val === "." && state.buffer.indexOf(val) !== -1) {
      return;
    }
    // Sau dấu phẩy thì không được nhiều hơn 2 ký tự
    if (state.buffer.charAt(state.buffer.length - 3) === ".") {
      return;
    }
    state.buffer += val;
  },
  resetClickMoneyCount(state) {
    state.clickMoneyCount = 0;
  },
  setPaymentNumber(state) {
    ++state.countPayment;
    if (state.countPayment == 5) {
      state.countPayment = 0;
      window.location.reload();
    }
  },
  resetBuffertemp(state) {
    state.buffertemp = '';
  }
};

export default {
  state,
  getters,
  actions,
  mutations,
};
