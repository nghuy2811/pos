import { globalStore } from "@/global/global.js";
import lzutf8 from "lzutf8";

const state = {
  // data
  allCategory: [],
  categoryDataForShow: [],
  categoryPaginated: [],
  dataBreadcrumbItem: [],
  isSearching: false,

  // biến xử lý phân trang
  recordPerPage: 8,
  startPage: 1,
  currentPage: 1,
  nextPage: 0,
  prevPage: 0,
  totalPage: 0,

  productForSearch: [],
};

const getters = {
  // computed
};

const actions = {
  // methods
  initCategoryDataWhenFirstLoad({ state, dispatch, rootState }, isClicked) {
    state.currentPage = 1;
    state.categoryDataForShow = [];
    state.categoryPaginated = [];

    state.dataBreadcrumbItem = [];

    for (let category of state.allCategory) {
      if (!category.parent_id) {
        state.categoryDataForShow.push(category);
      }
    }

    // Do pagination
    state.categoryPaginated = globalStore.doPaginate(
      state.categoryDataForShow,
      state.recordPerPage,
      state.currentPage
    );

    if (isClicked) {
      // handle list product filter
      dispatch("filterProductByCategory", ["", true]);
    }
  },
  handlePaginateNumberIndex({ state, dispatch, rootState }, currentPageData) {
    // hiển thị page index khi load page
    state.totalPage = Math.ceil(
      state.categoryDataForShow.length / state.recordPerPage
    );
    state.nextPage = currentPageData + 1;
    state.prevPage = currentPageData - 1;

    if (state.prevPage === 0) {
      state.prevPage = state.totalPage;
    }
    if (state.currentPage === state.totalPage) {
      state.nextPage = state.startPage;
    }
  },
  nextPageCategory({ commit, state, dispatch }) {
    if (
      state.currentPage <
      globalStore.numPages(state.categoryDataForShow, state.recordPerPage)
    ) {
      state.currentPage++;

      commit("changePage", state.currentPage);
    } else {
      state.currentPage = 1;
      commit("changePage", state.currentPage);
    }

    dispatch("handlePaginateNumberIndex", state.currentPage);
  },
  prevPageCategory({ commit, state, dispatch }) {
    if (state.currentPage > 1) {
      state.currentPage--;

      commit("changePage", state.currentPage);
    } else {
      state.currentPage--;
      if (state.currentPage < 1) {
        state.currentPage = Math.ceil(
          state.categoryDataForShow.length / state.recordPerPage
        );

        commit("changePage", state.currentPage);
      }
    }

    dispatch("handlePaginateNumberIndex", state.currentPage);
  },
  doClickToCategoryItem({ commit, state, dispatch, rootState }, item) {
    let tempCategoryFilter = [];
    state.categoryDataForShow = [];

    //handle list category
    for (let category of state.allCategory) {
      if (category.parent_id === item.id) {
        tempCategoryFilter.push(category);
      }
    }
    if (tempCategoryFilter.length > 0) {
      state.dataBreadcrumbItem = [];

      state.currentPage = 1;
      state.categoryDataForShow = tempCategoryFilter;

      state.categoryPaginated = globalStore.doPaginate(
        state.categoryDataForShow,
        state.recordPerPage,
        state.currentPage
      );
    }

    //handle breadcrumb data
    if (state.dataBreadcrumbItem.length > 0) {
      if (!state.dataBreadcrumbItem.includes(item)) {
        state.dataBreadcrumbItem.push(item);

        //remove object has the same level if it's exit
        for (let i = 0; i < state.dataBreadcrumbItem.length; i++) {
          if (
            state.dataBreadcrumbItem[i].parent_id === item.parent_id &&
            state.dataBreadcrumbItem[i].id !== item.id
          ) {
            state.dataBreadcrumbItem.splice(i, 1);
            break;
          }
        }
      }
    } else {
      state.dataBreadcrumbItem.push(item);
    }

    //handle list product filter
    dispatch("filterProductByCategory", [item, false]);
  },
  filterProductByCategory(
    { commit, state, rootState },
    [category, isRootCategory]
  ) {
    state.isSearching = true;
    if (!isRootCategory) {
      let tempProducts = [];

      for (let product of rootState.product.allProduct) {
        if (parseInt(product.category_id) === parseInt(category.id)) {
          tempProducts.push(product);
        }
      }

      for (let product of rootState.product.allProduct) {
        for (let catt of state.categoryDataForShow) {
          if (parseInt(product.category_id) === parseInt(catt.id)) {
            tempProducts.push(product);
          }
        }
      }

      commit("setProductsFiltering", tempProducts);
      commit("setDataProductLazyLoad", rootState.product.productsFiltering);
    } else {
      state.isSearching = false;
      commit("setDataProductLazyLoad", rootState.product.allProduct);
    }
  },
  filterProductByWord({ commit, dispatch, state, rootState }, [seachval]) {
    if (seachval.length > 0) {
      state.isSearching = true;

      let tempProducts = [];
      let strSearch = [];

      strSearch = globalStore.createDataFullTextSearch(seachval);
      // console.log("strSearch:", strSearch);
      // rootState.product.allProduct = JSON.parse(JSON.parse(JSON.parse(localStorage.getItem(globalStore.storageNameAllProduct))).value);
      for (let product of state.productForSearch) {
        let str = product.full_name_search + product.barcode;
        // let str = product.full_name_search + " " + product.barcode;
        // let match = true;

        if (product.kartonbarcode !== false) {
          // str = str + " " + product.kartonbarcode;
          str = str + product.kartonbarcode;
        }
        if (product.packungbarcode !== false) {
          // str = str + " " + product.packungbarcode;
          str = str + product.packungbarcode;
        }

        if (str.includes(seachval)) {
          tempProducts.push(product);
        }
      }
      commit("setProductsFiltering", tempProducts);
      commit("setDataProductLazyLoad", rootState.product.productsFiltering);
      // xử lý màu background của sản phẩm
      setTimeout(() => {
        let list = document.querySelector(".category-child-items");
        let items = list.querySelectorAll(".single-product-item");
        globalStore.handleAlternateColorButton(list, items, 6);
      }, 80);
    } else {
      state.isSearching = false;
      commit("setDataProductLazyLoad", rootState.product.allProduct);
      // rootState.product.allProductLazyLoad = JSON.parse(JSON.parse(JSON.parse(localStorage.getItem(globalStore.storageNameAllProduct))).value);
    }

    dispatch("resetPagerProductList");
  },
  async setProductByBarcode({ commit, dispatch, state, rootState }, barcode) {
    let barcode_temp = '0000000000000' + barcode.toString();
    barcode_temp = barcode_temp.substring(barcode_temp.length-13, barcode_temp.length);
    if (barcode_temp.substring(0, 1) === '0') {
      let an = barcode_temp.substring(0, barcode_temp.length-6);
      let check_found = false;
      for (let product of rootState.product.allProduct) {
        if (parseInt(product.artikel_nummer.toString().replace(/\s/g, "")) === parseInt(an)) {
          let cate_quacan = [];
          let category_can = [257];
          for (let i = 0; i < state.allCategory.length; i++) {
            if(category_can.includes(state.allCategory[i]['id']) || category_can.includes(state.allCategory[i]['parent_id'])){
              cate_quacan.push(state.allCategory[i]['id'].toString())
            }
          }
          if(cate_quacan.includes(product['category_id'].toString())){
            await dispatch("doAddProductToOrderListKg", product);
            rootState.product.buffer = '';
            let new_qty = barcode_temp.toString().substring(barcode_temp.length-6, barcode_temp.length-1);
            dispatch('setPreisValue', product.sale_price || 0);
            if (product.uom === "kg") {
              dispatch('setMengeValue', parseInt(new_qty)/1000);
            }else{
              dispatch('setMengeValue', parseInt(new_qty));
            }
            check_found = true;
          }
        }
      }
      if(check_found){
        dispatch("doCalculation");
        return;
      }
    }
    if (barcode.toString().substring(0, 7) === "2400120") {
      var khoiluong = barcode.toString().substring(7, 12);
      for (let product of rootState.product.allProduct) {
        if (product.barcode.toString().replace(/\s/g, "") === "2400120") {
          dispatch("doSelectProductToOrder", product);
        }
      }
      rootState.product.buffer = "";
      rootState.product.statebuffer = "preis";
      dispatch("handleClickNumberBoardBarcodethitca", parseFloat(parseFloat(khoiluong) / 100).toFixed(2) || 0);
      return;
    } else {
      for (let product of rootState.product.allProduct) {
        if (product.barcode.toString().includes(barcode) === true) {
          dispatch("doAddProductToOrderListBarcode", product);
          return;
        }
        if (product.kartonbarcode.toString().includes(barcode) === true) {
          let product_selected = { ...product };
          product_selected["uom"] = "Karton";
          dispatch("doAddProductToOrderListBarcode", product_selected);
          dispatch("handleChangeKarton");
          return;
        }
        if (product.packungbarcode.toString().includes(barcode) === true) {
          let product_selected = { ...product };
          product_selected["uom"] = "Umpackung";
          dispatch("doAddProductToOrderListBarcode", product_selected);
          dispatch("handleChangePackung");
          return;
        }
      }
      rootState.popup.warningContent = globalStore.$i18n.t("Barcode not found");
      globalStore.showPopup("showWarning");
      return;
    }
  },
  setProductForSearch({ commit, dispatch, state, rootState }) {
    //   state.productForSearch = JSON.parse(JSON.parse(JSON.parse(localStorage.getItem(globalStore.storageNameAllProduct))).value);
    state.productForSearch = JSON.parse(
      localStorage.getItem(globalStore.storageNameAllProduct)
    );

    lzutf8.decompressAsync(
      state.productForSearch,
      { inputEncoding: "StorageBinaryString", outputEncoding: "String" },
      (resultProducts, errorProducts) => {
        if (errorProducts === undefined) {
          // console.log('after decompress:', JSON.parse(JSON.parse(resultProducts).value));
          state.productForSearch = JSON.parse(resultProducts).value;
          for (let product of state.productForSearch) {
            let str = product.full_name;
            str = str.toString().toLowerCase();
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|ä/g, "a");
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
            str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|ö/g, "o");
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|ü/g, "u");
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
            str = str.replace(/đ/g, "d");
            str = str.replace(/ + /g, " ");
            str = str.trim();

            product.full_name_search = str;
          }
        } else {
          console.log("Decompression error: " + errorProducts.message);
        }
      }
    );
  },
};

const mutations = {
  // handle response from actions to update state
  setDataCategory(state, allCategory) {
    state.allCategory = allCategory;
  },
  changePage(state, page) {
    state.categoryPaginated = globalStore.doPaginate(
      state.categoryDataForShow,
      state.recordPerPage,
      page
    );
  },
  initCurrentPageIndex(state) {
    state.startPage = 1;
  },
  setStateSearching(state) {
    state.isSearching = false;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
