import Vue from 'vue'
import Vuex from 'vuex'
import category from './modules/category'
import product from './modules/product'
import customer from './modules/customer'
import payment from './modules/payment'
import listOrdered from './modules/listOrdered'
import popup from './modules/popup'
import i18n from './modules/i18n'
import scroll from './modules/scroll'
import helper from './modules/helper'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    category,
    product,
    customer,
    payment,
    listOrdered,
    popup,
    i18n,
    scroll,
    helper
  },
  state: {

  },

  getters: {

  },

  actions: {

  },

  mutations: {

  }
})
