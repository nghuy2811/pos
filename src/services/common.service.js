import Vue from 'vue'
import axios from 'axios'
import cookies from 'vue-cookies'
import router from '@/router'

import {
  globalStore
} from '../global/global.js'

export default {
  getSingleRequest(target, body) {
    return axios.post(target, body);
  },
  storeToLocalStorage(name, data) {
    localStorage.removeItem(name);
    localStorage.setItem(name, JSON.stringify(data));
  },
  removeLocalStorageItem(name) {
    localStorage.removeItem(name);
  },
  checkLocalStorageItemExisted(name) {
    let localCollection = localStorage.getItem(name);
    localCollection = JSON.parse(localCollection);
    if (!localCollection) {
      return false;
    }

    return localCollection;
  },
  checkValid(value, lengthCheck = true) {
    if (typeof value !== 'undefined' && value !== null) {
      const stringValue = value.toString();
      if (typeof value === 'function' || typeof value === 'object') {
        if (lengthCheck) {
          return !(typeof value.length !== 'undefined' && value.length <= 0)
        }

        return true;
      }

      if (typeof (value) === 'boolean') {
        return value;
      }

      if (/\S/.test(stringValue) && !isNaN(stringValue)) {
        return true;
      }

      if (/\S/.test(stringValue)) {
        return true;
      }
    }

    return false;
  },
  // handleLoginState(name) {
  //   let cookieLogin = cookies.get(name);
  //   if (!this.checkValid(cookieLogin)) {
  //     localStorage.clear();
  //
  //     router.push({
  //       path: '/'
  //     });
  //   }
  // }
  // doPaginate(array, pageSize, pageNumber) {
  //   --pageNumber;
  //   // console.log('aa:',array.slice(pageNumber * pageSize, (pageNumber + 1) * pageSize));
  //   return array.slice(pageNumber * pageSize, (pageNumber + 1) * pageSize);
  // },
  // numPages(obj, recordPerPage) {
  //   return Math.ceil(obj.length / recordPerPage);
  // }
}
