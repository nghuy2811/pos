import Vue from 'vue'
import axios from 'axios'
import {
  globalStore
} from '../global/global.js'

export default {
  getAll() {
    axios.get('https://jsonplaceholder.typicode.com/comments')
      .then(function (response) {
        console.log('all list product:', response);
      })
      .catch(function (error) {
        console.log(error);
      });
  }
}
