import Vue from "vue";
import store from "@/store";
import router from "@/router";
import io from "socket.io-client";
import VueCookies from "vue-cookies";
import i18n from "@/i18n";
import axios from "axios";

Vue.use(VueCookies);

export const globalStore = new Vue({
  store,
  router,
  i18n,
  data: {
    baseUrl: "",
    db: "pos",
    new_url: "192.168.1.20:8070",
    cookieNameLoginInfo: "loginPOS",
    storageNameAllProduct: "allProduct",
    storageNameAllCategory: "allCategory",
    storageNameAllCustomer: "allCustomer",
    storageCurrentLang: "currentLang",
    socket: io(global_socket, {
      reconnection: true,
      reconnectionDelay: 1000,
      reconnectionDelayMax: 5000,
      reconnectionAttempts: Infinity,
    }),
    socketcheck: null,
  },
  methods: {
    async getOrderSyncServer() {
      let userinfo = JSON.parse(
        localStorage.getItem(globalStore.cookieNameLoginInfo)
      ).value[0];
      const postBody = { params: { username: userinfo.username.toString() } };
      let response = await axios.post(
        `${globalStore.baseUrl}socket/getorderlist`,
        postBody
      );
      let orders =
        response.data.result.length === 0
          ? []
          : JSON.parse(response.data.result);
      return orders;
    },
    async getMaxOrder() {
      let currentUser = JSON.parse(
        localStorage.getItem(globalStore.cookieNameLoginInfo)
      ).value[0];
      const postBody = { params: { user_login: currentUser["username"] } };
      let response = await axios.post(
        `${globalStore.baseUrl}user/getmaxorder`,
        postBody
      );
      let max_order = JSON.parse(response.data.result);
      max_order = JSON.parse(max_order.value);
      max_order = max_order[0]["max_order"];
      if (max_order === null) {
        max_order = 1;
      }
      return max_order;
    },
    checkValid(value, lengthCheck = true) {
      if (typeof value !== "undefined" && value !== null) {
        const stringValue = value.toString();
        if (typeof value === "function" || typeof value === "object") {
          if (lengthCheck) {
            return !(typeof value.length !== "undefined" && value.length <= 0);
          }

          return true;
        }

        if (typeof value === "boolean") {
          return value;
        }

        if (/\S/.test(stringValue) && !isNaN(stringValue)) {
          return true;
        }

        if (/\S/.test(stringValue)) {
          return true;
        }
      }

      return false;
    },
    doPaginate(array, pageSize, pageNumber) {
      --pageNumber;
      // console.log('aa:',array.slice(pageNumber * pageSize, (pageNumber + 1) * pageSize));
      return array.slice(pageNumber * pageSize, (pageNumber + 1) * pageSize);
    },
    numPages(obj, recordPerPage) {
      return Math.ceil(obj.length / recordPerPage);
    },
    scrollToElement(parentEleClassName, eleClassName, indexEle) {
      document.getElementsByClassName(parentEleClassName)[0].scrollTop =
        document.getElementsByClassName(eleClassName)[indexEle].offsetTop -
        document.getElementsByClassName(parentEleClassName)[0].offsetTop;
    },
    isObjectEmpty(obj) {
      for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
          return false;
        }
      }
      return true;
    },
    getCurrentUser() {
      return JSON.parse(localStorage.getItem(globalStore.cookieNameLoginInfo))
        .value[0];
    },
    async sendSocket(strorder, username) {
      clearTimeout(this.socketcheck);
      this.socketcheck = setTimeout(async () => {
        const body = { params: { username: username } };
        let objorder = JSON.parse(strorder);
        let responsecheck = await axios.post(
          `${globalStore.baseUrl}socket/getordertotal`,
          body
        );
        let responsecheckobj = JSON.parse(responsecheck.data.result);
        if (
          responsecheckobj["total_with_discount"] !==
            objorder["total_with_discount"] ||
          responsecheckobj["total_give"] !== objorder["total_give"]
        ) {
          const postBody = { params: { order: strorder, username: username } };
          let response = await axios.post(
            `${globalStore.baseUrl}socket/setorder`,
            postBody
          );

          const postOrderList = {
            params: {
              order: JSON.stringify(globalStore.$store.state.product.orders),
              username: username,
            },
          };
          await axios.post(
            `${globalStore.baseUrl}socket/setorderlist`,
            postOrderList
          );

          if (response.data.result.sucessfull) {
            await globalStore.socket.emit("order", { username: username });
          }
        } else {
          await globalStore.socket.emit("order", { username: username });
        }
      }, 10);
    },
    doPrintIP() {
      this.userinfo = JSON.parse(
        localStorage.getItem(globalStore.cookieNameLoginInfo)
      ).value[0];
      const postBody = {
        params: {
          userid: this.userinfo.userid,
        },
      };
      axios.post(`${globalStore.baseUrl}user/printtoip`, postBody);
    },
    doPrintIPWithoutOpen() {
      this.userinfo = JSON.parse(
        localStorage.getItem(globalStore.cookieNameLoginInfo)
      ).value[0];
      const postBody = {
        params: {
          userid: this.userinfo.userid,
        },
      };
      axios.post(
        `${globalStore.baseUrl}user/printtoipwithoutopencashdraw`,
        postBody
      );
    },
    showPopup(type) {
      document
        .getElementsByClassName("popup-common")[0]
        .classList.add("active");

      if (type === "priceInfo") {
        let priceInfoBox = document.querySelector(".type-priceInfo");
        priceInfoBox.style.display = "block";
      }

      if (type === "showWarning") {
        let popups = document.querySelectorAll(".popup-type");
        for (let popup of popups) {
          popup.removeAttribute("style");
        }

        let showWarning = document.querySelector(".type-showWarning");
        showWarning.style.display = "block";
      }

      if (type === "warningVollRetoure") {
        let priceInfoBox = document.querySelector(".type-warningVollRetoure");
        priceInfoBox.style.display = "block";
      }

      if (type === "warningComma") {
        let priceInfoBox = document.querySelector(".type-warningComma");
        priceInfoBox.style.display = "block";
      }

      if (type === "warningNoProduct") {
        let priceInfoBox = document.querySelector(".type-warningNoProduct");
        priceInfoBox.style.display = "block";
      }

      if (type === "warningNotStorno") {
        let priceInfoBox = document.querySelector(".type-warningNotStorno");
        priceInfoBox.style.display = "block";
      }

      if (type === "confirmPopup") {
        let priceInfoBox = document.querySelector(".type-confirm");
        priceInfoBox.style.display = "block";
      }

      if (type === "confirmClear") {
        let priceInfoBox = document.querySelector(".type-confirmClear");
        priceInfoBox.style.display = "block";
      }

      if (type === "confirmAbschluss") {
        let priceInfoBox = document.querySelector(".type-confirmAbschluss");
        priceInfoBox.style.display = "block";
      }

      if (type === "confirmCardPay") {
        let priceInfoBox = document.querySelector(".type-confirm-cardpay");
        priceInfoBox.style.display = "block";
      }

      if (type === "confirmLogoutPopup") {
        let priceInfoBox = document.querySelector(".type-confirm-logout");
        priceInfoBox.style.display = "block";
      }

      if (type === "confirmRetoureAllPopup") {
        let priceInfoBox = document.querySelector(".type-retoure-all-select");
        priceInfoBox.style.display = "block";
      }

      if (type === "confirmRetourePartPopup") {
        let priceInfoBox = document.querySelector(".type-retoure-part-select");
        priceInfoBox.style.display = "block";
      }

      if (type === "passwordRetoure") {
        let priceInfoBox = document.querySelector(".type-password-retoure");
        priceInfoBox.style.display = "block";
      }

      if (type === "passwordOffer") {
        let priceInfoBox = document.querySelector(".type-password-offer");
        priceInfoBox.style.display = "block";
      }

      if (type === "passwordOfferReload") {
        let priceInfoBox = document.querySelector(
          ".type-password-offer-reload"
        );
        priceInfoBox.style.display = "block";
      }

      if (type === "passwordStorno") {
        let priceInfoBox = document.querySelector(".type-password-storno");
        priceInfoBox.style.display = "block";
      }

      if (type === "passwordStornoAll") {
        let priceInfoBox = document.querySelector(".type-passwordStornoAll");
        priceInfoBox.style.display = "block";
      }

      if (type === "requirePasswordZahlungswechsel") {
        let priceInfoBox = document.querySelector(
          ".type-require-password-zahlungswechsel"
        );
        priceInfoBox.style.display = "block";
      }

      if (type === "requirePassword") {
        let priceInfoBox = document.querySelector(".type-require-password");
        priceInfoBox.style.display = "block";
      }

      if (type === "requirePasswordNewOrder") {
        let priceInfoBox = document.querySelector(
          ".type-require-password-new-order"
        );
        priceInfoBox.style.display = "block";
      }

      if (type === "couponShow") {
        let priceInfoBox = document.querySelector(".type-coupon-show");
        priceInfoBox.style.display = "block";
      }

      if (type === "requirePasswordDiscountLine") {
        let priceInfoBox = document.querySelector(
          ".type-require-password-discountline"
        );
        priceInfoBox.style.display = "block";
      }

      if (type === "requirePasswordDiscountTotal") {
        let priceInfoBox = document.querySelector(
          ".type-require-password-discounttotal"
        );
        priceInfoBox.style.display = "block";
      }
      if (type === "requirePasswordPayment") {
        let priceInfoBox = document.querySelector(
          ".type-require-password-payment"
        );
        priceInfoBox.style.display = "block";
      }
      if (type === "requirePasswordOfferPayment") {
        let priceInfoBox = document.querySelector(
          ".type-require-password-offer-payment"
        );
        priceInfoBox.style.display = "block";
      }

      if (type === "confirmCloseBrowser") {
        let closeBrowserBox = document.querySelector(
          ".type-confirm-close-browser"
        );
        closeBrowserBox.style.display = "block";
      }

      if (type === "productExpendPrice") {
        let productExpendPriceBox = document.querySelector(
          ".type-product-expend-price"
        );
        productExpendPriceBox.style.display = "block";
      }

      if (type === "productTotalDiscountinput") {
        let productExpendPriceBox = document.querySelector(
          ".type-product-totaldiscountinput"
        );
        productExpendPriceBox.style.display = "block";
      }

      if (type === "productKg") {
        this.$store.commit("handleCanClickBtnConfirmPopup", true);
        let productKg = document.querySelector(".type-product-kg");
        productKg.style.display = "block";
      }

      if (type === "productDiv") {
        this.$store.commit("handleCanClickBtnConfirmPopup", true);
        let productDiv = document.querySelector(".type-product-div");
        productDiv.style.display = "block";
      }

      if (type === "invalidTotalGive") {
        let invalidTotalGive = document.querySelector(
          ".type-invalid-totalgive"
        );
        invalidTotalGive.style.display = "block";
      }

      if (type === "warningUmpackung") {
        let warningUmpackung = document.querySelector(
          ".type-warning-umpackung"
        );
        warningUmpackung.style.display = "block";
      }

      if (type === "warningKarton") {
        let warningKarton = document.querySelector(".type-warning-karton");
        warningKarton.style.display = "block";
      }

      if (type === "increaseQuantity") {
        let increaseQuantity = document.querySelector(
          ".type-increase-quantity"
        );
        increaseQuantity.style.display = "block";
      }
    },
    confirmPopup() {
      this.$store.dispatch("confirmPopup");
      this.closePopup("");
    },
    closeConfirmPopup() {
      this.$store.dispatch("closeConfirmPopup");
    },
    closePopup(type) {
      document
        .getElementsByClassName("popup-common")[0]
        .classList.remove("active");

      let popupType = document.querySelectorAll(".popup-type");
      popupType.forEach((el, index) => {
        setTimeout(() => {
          el.style.display = "none";
        }, 600);
      });
    },
    initHightLightOrder() {
      setTimeout(() => {
        let rowProduct = document.getElementsByClassName(
          "single-row-product-order"
        );
        if (rowProduct.length > 0) {
          for (let row of rowProduct) {
            row.classList.remove("isClicked");
          }

          rowProduct[0].classList.add("isClicked");
        }
      }, 10);
    },
    detectDevice() {
      if (navigator.appVersion.indexOf("Win") != -1) {
        document.body.classList.add("is-window");
      }

      if (
        navigator.userAgent.match(/Android/i) ||
        navigator.userAgent.match(/webOS/i) ||
        navigator.userAgent.match(/iPhone/i) ||
        navigator.userAgent.match(/iPad/i) ||
        navigator.userAgent.match(/iPod/i) ||
        navigator.userAgent.match(/BlackBerry/i) ||
        navigator.userAgent.match(/Windows Phone/i)
      ) {
        document.body.classList.add("is-device");
      } else {
        document.body.classList.remove("is-device");
      }
    },
    handleScrollContent(target, direction, disantScrollParam) {
      let currentPoint;
      let startPoint = 0;

      if (direction === "down") {
        currentPoint =
          this.$store.state.scroll.currentPoint + disantScrollParam;
      }
      if (direction === "up") {
        currentPoint =
          this.$store.state.scroll.currentPoint - disantScrollParam;
        if (currentPoint < 0) {
          currentPoint = startPoint;
        }
      }

      this.$store.dispatch("updateCurrentPoint", currentPoint);

      document.querySelector(
        target
      ).scrollTop = this.$store.state.scroll.currentPoint;
    },
    handleAlternateColorButton(list, items, itemsInRow) {
      let arr1 = [];
      let arr2 = [];

      for (let i = 0; i < items.length; i++) {
        if (Math.floor(i / itemsInRow) % 2 === 0) {
          arr1.push(items[i]);
        } else {
          arr2.push(items[i]);
        }
      }

      for (let i = 0; i < arr1.length; i++) {
        if (i % 2 === 0) {
          arr1[i].classList.add("item-type-1");
        } else {
          arr1[i].classList.add("item-type-2");
        }
      }
      for (let i = 0; i < arr2.length; i++) {
        if (i % 2 === 0) {
          arr2[i].classList.add("item-type-2");
        } else {
          arr2[i].classList.add("item-type-1");
        }
      }
    },
    async userLogout() {
      if (this.$router.currentRoute.name !== "Login") {
        let userinfo = JSON.parse(
          localStorage.getItem(globalStore.cookieNameLoginInfo)
        ).value[0];
        const postBody = { params: { username: userinfo.username.toString() } };
        let response = await axios.post(
          `${globalStore.baseUrl}socket/getorderlist`,
          postBody
        );
        let localCollection = JSON.parse(response.data.result);

        for (let i = 0; i < localCollection.length; i++) {
          if (localCollection[i].lines.length > 0) {
            return;
          }
        }

        this.$cookies.remove(globalStore.cookieNameLoginInfo);
        localStorage.removeItem(globalStore.cookieNameLoginInfo);
        globalStore.socket.emit("login", "reload");

        if (this.$router.currentRoute.name === "Customer") {
          this.$router.push({
            path: "customer",
          });
        } else {
          this.$router.push({
            path: "/",
          });
        }
      }
    },
    setCharAt(str, index, chr) {
      if (index > str.length - 1) return str;
      return str.substr(0, index) + chr + str.substr(index + 1);
    },
    initSlider(target, timeOut) {
      let slider = document.querySelectorAll(target);

      if (slider.length > 0) {
        slider.forEach((el, index) => {
          let sliderItems = el.querySelectorAll(".pos-slider-item");
          if (sliderItems.length > 0) {
            let totalItems = sliderItems.length;
            let currentItemIsActive = 0;

            sliderItems[0].classList.add("is-active");
            setInterval(() => {
              if (
                sliderItems[currentItemIsActive].classList.contains("is-active")
              ) {
                currentItemIsActive =
                  Number(
                    sliderItems[currentItemIsActive].getAttribute(
                      "data-slider-item"
                    )
                  ) + 1;
                if (currentItemIsActive > totalItems - 1) {
                  currentItemIsActive = 0;
                }
                sliderItems.forEach((elItem, indexItem) => {
                  elItem.classList.remove("is-active");
                });
                sliderItems[currentItemIsActive].classList.add("is-active");
              }
            }, timeOut);
          }
        });
      }
    },
    isInViewport(elements, spacingBottom) {
      var scroll = window.scrollY || window.pageYOffset;
      var boundsTop = elements.getBoundingClientRect().top + scroll;

      var viewport = {
        top: scroll,
        bottom: scroll + window.innerHeight - spacingBottom,
      };

      var bounds = {
        top: boundsTop,
        bottom: boundsTop + elements.clientHeight,
      };

      return (
        (bounds.bottom >= viewport.top && bounds.bottom <= viewport.bottom) ||
        (bounds.top <= viewport.bottom && bounds.top >= viewport.top)
      );
    },
    domExists(node) {
      return node === document.body ? false : document.body.contains(node);
    },

    sortPrintOrderLine(arr, isReprint = true) {
      if (isReprint) {
        arr.sort((x, y) => {
          if (x.product_id < y.product_id) {
            return -1;
          }

          if (x.product_id > y.product_id) {
            return 1;
          }

          if (Number(x.total_price) > 0) {
            return -1;
          }

          return 0;
        });
      } else {
        arr.sort((x, y) => {
          if (x.id < y.id) {
            return -1;
          }

          if (x.id > y.id) {
            return 1;
          }

          if (Number(x.total_price) > 0) {
            return -1;
          }

          return 0;
        });
      }
    },

    createDataFullTextSearch(strInput) {
      let tmpStr = strInput;
      // let data = [];

      tmpStr = tmpStr
        .toLowerCase()
        .replace(/\W/g, " ")
        .replace(/\s+/g, " ")
        .trim()
        .split(" ");

      // if (tmpStr.length > 1) {
      //   for (let i = 0; i < tmpStr.length; i++) {
      //     for (let j = 0; j < tmpStr.length; j++) {
      //       if (tmpStr[i] != tmpStr[j]) {
      //         data.push(`${tmpStr[i]} ${tmpStr[j]}`);
      //       }
      //     }
      //   }
      // } else {
      //   data = [...tmpStr];
      // }

      // console.log("data:", data);

      return tmpStr;
    },
  },
});
