import Vue from "vue";

Vue.filter("currencyFormat", function(value) {
  return Number(value) > 0
    ? value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    : 0;
});

Vue.filter("centToEuroFormat", function(value) {
  return Number(value) > 0
    ? (Number(value) / 100).toLocaleString("de-DE", {
        style: "currency",
        currency: "EUR",
        minimumFractionDigits: 2,
      })
    : 0;
});

Vue.filter("kgFormat", function(value) {
  return Number(value) > 0 ? (Number(value) / 1000).toLocaleString("de-DE") : 0;
});

Vue.filter("kgQuantityFormat", function(value) {
  if (Number(value) === 0) return 0;

  return Number(value) > 0
    ? (Number(value) / 1000).toLocaleString("de-DE")
    : "";
});
