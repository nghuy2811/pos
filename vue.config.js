module.exports = {
  publicPath: '/',
  assetsDir: 'static',
  productionSourceMap: false,
  runtimeCompiler: true,
  configureWebpack:{
    mode: 'production',
    optimization: {
      splitChunks: {
        minSize: 10000,
        maxSize: 750000,
      }
    }
  }
};